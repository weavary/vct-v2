<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/
if(Auth::check()){

	View::share('nav_site', People::find(Auth::user()->id));
}


Route::get('/q=', array(
	'as' => 'home',
	'uses' => 'HomeController@home'	
));

Route::get('/', array(
	'as' => 'home',
	'uses' => 'HomeController@home'	
));

Route::get('/people/id={id}', array(
	'as' => 'profile-people',
	'uses' => 'PeopleController@people'
));

Route::get('/site/{site_name}/{id}', array(
	'as' => 'profile-site',
	'uses' => 'SiteController@site',
));

Route::get('/event/{id}', array(
	'as' => 'profile-event',
	'uses' => 'EventController@event'
));

Route::get('/event/{id}/payment={amount}', array(
	'as' => 'payment-event',
	'uses' => 'EventController@getEventPayment'
));

Route::get('/q={keyword}', array(
	'as' => 'search',
	'uses' => 'HomeController@getSearch'
));

Route::get('/events/all', array(
	'as' => 'all-event',
	'uses' => 'EventController@allEvent'
));

Route::get('/events/list', array(
	'as' => 'list-event',
	'uses' => 'EventController@listEvent'
));

Route::get('/api/events', array(
	'as' => 'api-events',
	'uses' => 'ApiController@getEvents'
));

Route::get('/api/tags', array(
	'as' => 'api-tags',
	'uses' => 'ApiController@getTags'
));

Route::get('/helps', array(
	'as' => 'helps',
	'uses' => 'HelpController@help'
));

Route::get('/helps/howtos', array(
	'as' => 'helps-howtos',
	'uses' => 'HelpController@howtos'
));

Route::get('/helps/faq', array(
	'as' => 'helps-faq',
	'uses' => 'HelpController@faq'
));

Route::get('/helps/feedback', array(
	'as' => 'helps-feedback',
	'uses' => 'HelpController@getFeedback'
));

Route::get('/policies', array(
	'as' => 'policies-tos',
	'uses' => 'PolicyController@tos'
));

Route::get('/ads', array(
	'as' => 'ads',
	'uses' => 'AdvertisingController@getAds'
));

Route::get('/about/team', array(
	'as' => 'about-team',
	'uses' => 'AboutController@team'
));


/*CSRF Protection Group*/
Route::group(array('before' => 'csrf'), function(){

	Route::post('/helps/feedback', array(
		'as' => 'helps-feedback-post',
		'uses' => 'HelpController@postFeedback'
	));

	Route::post('/ads', array(
		'as' => 'ads-post',
		'uses' => 'AdvertisingController@postAds'
	));

	Route::post('/q=', array(
		'as' => 'search-post',
		'uses' => 'HomeController@postSearch',
	));

	Route::post('/event/{id}/payment={amount}', array(
		'as' => 'payment-event-post',
		'uses' => 'EventController@postEventPayment'
	));

	

});

/* Unauthenticated Group */
Route::group(array('before' => 'guest'), function(){

	/* Create Account (GET) */
	Route::get('/join', array(
		'as' => 'join',
		'uses' => 'HomeController@getJoin'
	));

	/*Activate account */
	Route::get('/activate/{code}', array(
		'as' => 'activate',
		'uses' => 'HomeController@getActivate'
	));

	/* User Login (GET) */
	Route::get('/login', array(
		'as' => 'login',
		'uses' => 'HomeController@getLogin'
	));

	Route::get('/forgot', array(
		'as' => 'forgot',
		'uses' => 'HomeController@getForgot'
	));

	Route::get('/recover/{code}', array(
		'as' => 'recover',
		'uses' => 'HomeController@getRecover'
	));

	


	/*CSRF Protection Group*/
	Route::group(array('before' => 'csrf'), function(){

		/* Create Account (POST) */
		Route::post('/join', array(
			'as' => 'join-post',
			'uses' => 'HomeController@postJoin'
		));

		/* User Login (POST) */
		Route::post('/login', array(
			'as' => 'login-post',
			'uses' => 'HomeController@postLogin'
		));

		Route::post('/forgot', array(
			'as' => 'forgot-post',
			'uses' => 'HomeController@postForgot'
		));

		Route::post('/help/feedback', array(
			'as' => 'help-feedback-post',
			'uses' => 'HelpController@postFeedback'
		));

	});
});

/* Authenticated Group */
Route::group(array('before' => 'auth'), function(){

	/* Sign out (GET) */
	Route::get('/logout', array(
		'as' => 'logout',
		'uses' => 'SettingController@logout'
	));

	/* Setting (GET) */
	Route::get('/settings', array(
		'as' => 'settings',
		'uses' => 'SettingController@settings'
	));

	/* Change Profile (GET) */
	Route::get('/settings/profile', array(
		'as' => 'settings-profile',
		'uses' => 'SettingController@getChangeProfile'
	));

	/* Change Password (GET) */
	Route::get('/settings/password', array(
		'as' => 'settings-password',
		'uses' => 'SettingController@getChangePassword'
	));

	/* Create Site (GET) */
	Route::get('/site/create', array(
		'as' => 'create-site',
		'uses' => 'SiteController@getCreateSite'
	));

	/* Edit Site (GET) */
	Route::get('/site/{site_name}/{id}/settings', array(
		'as' => 'edit-site',
		'uses' => 'SiteController@getSettingsSite'
	));

	/* Edit Event (GET) */
	Route::get('/event/{id}/settings', array(
		'as' => 'edit-event',
		'uses' => 'EventController@getSettingsEvent'
	));

	/* Crop People Image (GET) */
	Route::get('/people/id={id}/crop', array(
		'as' => 'crop-photo-people',
		'uses' => 'PeopleController@getCropPhotoPeople'
	));

	/* Crop Site Image (GET) */
	Route::get('/site/{site_name}/{id}/crop', array(
		'as' => 'crop-photo-site',
		'uses' => 'SiteController@getCropPhotoSite'
	));

	/* Crop Event Image (GET) */
	Route::get('/event/{id}/crop', array(
		'as' => 'crop-photo-event',
		'uses' => 'EventController@getCropPhotoEvent'
	));

	

	/*CSRF Protection Group*/
	Route::group(array('before' => 'csrf'), function(){

		/* Change Profile (POST) */
		Route::post('/settings/profile', array(
			'as' => 'settings-profile-post',
			'uses' => 'SettingController@postChangeProfile'
		));

		/* Change Password (POSTS) */
		Route::post('/settings/password', array(
			'as' => 'settings-password-post',
			'uses' => 'SettingController@postChangePassword'
		));

		/* Create Account (POST) */
		Route::post('/site/create', array(
			'as' => 'create-site-post',
			'uses' => 'SiteController@postCreateSite'
		));

		Route::post('/site/{site_name}/{id}/settings', array(
			'as' => 'edit-site-post',
			'uses' => 'SiteController@postSettingsSite'
		));

		Route::post('/event/{site_id}/create', array(
			'as' => 'create-event-post',
			'uses' => 'EventController@postCreateEvent'
		));

		Route::post('/event/{id}/settings', array(
			'as' => 'edit-event-post',
			'uses' => 'EventController@postSettingsEvent'
		));

		Route::post('/people/id={id}', array(
			'as' => 'upload-photo-people-post',
			'uses' => 'PeopleController@postUploadPhotoPeople'
		));

		Route::post('/people/id={id}/crop', array(
		'as' => 'crop-photo-people-post',
		'uses' => 'PeopleController@postCropPhotoPeople'
		));

		Route::post('/site/{site_name}/{id}', array(
			'as' => 'upload-photo-site-post',
			'uses' => 'SiteController@postUploadPhotoSite'
		));

		Route::post('/site/{site_name}/{id}/crop', array(
		'as' => 'crop-photo-site-post',
		'uses' => 'SiteController@postCropPhotoSite'
		));

		Route::post('/event/{id}', array(
			'as' => 'upload-photo-event-post',
			'uses' => 'EventController@postUploadPhotoEvent'
		));

		Route::post('/event/{id}/crop', array(
		'as' => 'crop-photo-event-post',
		'uses' => 'EventController@postCropPhotoEvent'
		));

	});
});


Route::group(array('prefix' => 'api'), function(){
    Route::post('Login', array(
    	'as' => 'LoginAPI',
    	'uses' => 'ApiController@postLogin'
	));
	
	Route::post('Search', array(
		'as' => 'SearchAPI',
		'uses' => 'ApiController@getSearch'
	));
	
	Route::post('ListEvent', array(
		'as' => 'ListEventAPI',
		'uses' => 'ApiController@listEvent'
	));
	
	Route::post('ListNearbySite', array(
		'as' => 'ListNearbySiteAPI',
		'uses' => 'ApiController@listNearby'
	));
	
	Route::post('ListLatestSite', array(
		'as' => 'ListLatestSiteAPI',
		'uses' => 'ApiController@listLatestSites'
	));
	
	Route::post('RandomFoodSite', array(
		'as' => 'RandomFoodSite',
		'uses' => 'ApiController@randomFoodSite'
	));
});

