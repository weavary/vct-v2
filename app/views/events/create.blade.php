<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
	        <h4 class="modal-title" id="myModalLabel">Create Event</h4>
	    </div>
	    <div class="modal-body">
		<form class="form-create-event" role="form" action="{{URL::route('create-event-post', array('site_id' => $site->id))}}" onsubmit="return validateForm()" method="post">
		
		<div class="form-group margin-create-event">
			<div class="input-group">
				<div class="input-group-addon">Event Name</div>
		        <input type="text" class="form-control" id="event_name" name="event_name" placeholder="" id="title">
			</div>
		</div>
		<strong id="error_event_name"></strong>
		<div class="form-group margin-create-event">
			<div class="input-group">
				<div class="input-group-addon">Details</div>
		        <textarea class="form-control" id="event_desc" rows="4" name="event_desc"></textarea>
			</div>
		</div>
		<strong id="error_event_desc"></strong>

		<div class="form-group margin-create-event">
			<div class="input-group">
				<div class="input-group-addon">Location</div>
		        <input type="text" class="form-control" id="location" name="location" placeholder="">
			</div>
		</div>
		<strong id="error_location"></strong>
		<div class="form-group margin-create-event">
			<div class="input-group">
				<div class="input-group-addon" >Start Date</div>
		        <input type="text" onChange="validate()" class="form-control datepicker" name="start_date" id="start_date" placeholder="dd/mm/yyyy">
			</div>

			<div class="input-group">
		        <div class="input-group-addon">Start Time</div>
		        <select class="form-control" onChange="validate()" name="start_time" id="start_time" style="border-radius:0;-webkit-appearance: none">
		        	<option value="00.00">12.00am</option>
		        	<option value="00.30">12.30am</option>
		        	<option value="01.00">1.00am</option>
					<option value="01.30">1.30am</option>
					<option value="02.00">2.00am</option>
					<option value="02.30">2.30am</option>
					<option value="03.00">3.00am</option>
					<option value="03.30">3.30am</option>
					<option value="04.00">4.00am</option>
					<option value="04.30">4.30am</option>
					<option value="05.00">5.00am</option>
					<option value="05.30">5.30am</option>
					<option value="06.00">6.00am</option>
					<option value="06.30">6.30am</option>
					<option value="07.00">7.00am</option>
					<option value="07.30">7.30am</option>
					<option value="08.00">8.00am</option>
					<option value="08.30">8.30am</option>
					<option value="09.00">9.00am</option>
					<option value="09.30">9.30am</option>
					<option value="10.00">10.00am</option>
					<option value="10.30">10.30am</option>
					<option value="11.00">11.00am</option>
					<option value="11.30">11.30am</option>
					<option value="12.00">12.00pm</option>
					<option value="12.30">12.30pm</option>
					<option value="13.00">1.00pm</option>
					<option value="13.30">1.30pm</option>
					<option value="14.00">2.00pm</option>
					<option value="14.30">2.30pm</option>
					<option value="15.00">3.00pm</option>
					<option value="15.30">3.30pm</option>
					<option value="16.00">4.00pm</option>
					<option value="16.30">4.30pm</option>
					<option value="17.00">5.00pm</option>
					<option value="17.30">5.30pm</option>
					<option value="18.00">6.00pm</option>
					<option value="18.30">6.30pm</option>
					<option value="19.00">7.00pm</option>
					<option value="19.30">7.30pm</option>
					<option value="20.00">8.00pm</option>
					<option value="20.30">8.30pm</option>
					<option value="21.00">9.00pm</option>
					<option value="21.30">9.30pm</option>
					<option value="22.00">10.00pm</option>
					<option value="22.30">10.30pm</option>
					<option value="23.00">11.00pm</option>
					<option value="23.30">11.30pm</option>

				</select>
			</div>
			<strong id="error_start_date"></strong>
			<div class="input-group">
				<div class="input-group-addon">End Date</div>
		        <input type="text" onChange="validate()" class="form-control datepicker" name="end_date" id="end_date" placeholder="dd-mm-yyyy">
			</div>
			<div class="input-group">
		        <div class="input-group-addon">End Time</div>
		        <select class="form-control" onChange="validate()" name="end_time" id="end_time" style="border-radius:0;-webkit-appearance: none">
		        	<option value="00.00">12.00am</option>
		        	<option value="00.30">12.30am</option>
		        	<option value="01.00">1.00am</option>
					<option value="01.30">1.30am</option>
					<option value="02.00">2.00am</option>
					<option value="02.30">2.30am</option>
					<option value="03.00">3.00am</option>
					<option value="03.30">3.30am</option>
					<option value="04.00">4.00am</option>
					<option value="04.30">4.30am</option>
					<option value="05.00">5.00am</option>
					<option value="05.30">5.30am</option>
					<option value="06.00">6.00am</option>
					<option value="06.30">6.30am</option>
					<option value="07.00">7.00am</option>
					<option value="07.30">7.30am</option>
					<option value="08.00">8.00am</option>
					<option value="08.30">8.30am</option>
					<option value="09.00">9.00am</option>
					<option value="09.30">9.30am</option>
					<option value="10.00">10.00am</option>
					<option value="10.30">10.30am</option>
					<option value="11.00">11.00am</option>
					<option value="11.30">11.30am</option>
					<option value="12.00">12.00pm</option>
					<option value="12.30">12.30pm</option>
					<option value="13.00">1.00pm</option>
					<option value="13.30">1.30pm</option>
					<option value="14.00">2.00pm</option>
					<option value="14.30">2.30pm</option>
					<option value="15.00">3.00pm</option>
					<option value="15.30">3.30pm</option>
					<option value="16.00">4.00pm</option>
					<option value="16.30">4.30pm</option>
					<option value="17.00">5.00pm</option>
					<option value="17.30">5.30pm</option>
					<option value="18.00">6.00pm</option>
					<option value="18.30">6.30pm</option>
					<option value="19.00">7.00pm</option>
					<option value="19.30">7.30pm</option>
					<option value="20.00">8.00pm</option>
					<option value="20.30">8.30pm</option>
					<option value="21.00">9.00pm</option>
					<option value="21.30">9.30pm</option>
					<option value="22.00">10.00pm</option>
					<option value="22.30">10.30pm</option>
					<option value="23.00">11.00pm</option>
					<option value="23.30">11.30pm</option>

				</select>
			</div>
			<strong id="error_end_date"></strong>

		</div>
		<div class="form-group margin-create-event">
			<button class="btn btn-primary btn-block" id="create-event">Create Event</button>
		</div>
		{{HTML::script('js/jquery-ui.js')}}

<script>
$( ".datepicker" ).datepicker({
	inline: true
});
// Hover states on the static widgets
$( "#dialog-link, #icons li" ).hover(
  function() {
    $( this ).addClass( "ui-state-hover" );
  },
  function() {
    $( this ).removeClass( "ui-state-hover" );
  }
);
</script>
<script>
	function validateForm(){
		var event_name = document.getElementById("event_name").value;
		var event_desc = document.getElementById("event_desc").value;
		var location = document.getElementById("location").value;
		var start_date = document.getElementById("start_date").value;

		if(event_name === ""){
			alert("The event name field is required.");
			return false;
		}
		if(event_name.length > 75){
			alert("The event name may not be greater than 75 characters.");
			return false;
		}
		if(event_desc === ""){
			alert("The details field is required.");
			return false;
		}
		if(location === ""){
			alert("The location field is required.");
			return false;
		}
		if(start_date === ""){
			alert("The start date field is required.");
			return false;
		}
	}
</script>
<script>
	function validate(){
		var splitDate = document.getElementById("start_date").value.split("/");
		var splitTime = document.getElementById("start_time").value.split(".");
		var startDate = new Date(splitDate[2], splitDate[1] - 1, splitDate[0], splitTime[0], splitTime[1]);
		splitDate = document.getElementById("end_date").value.split("/");
		splitTime = document.getElementById("end_time").value.split(".");
		var endDate = new Date(splitDate[2], splitDate[1] - 1, splitDate[0], splitTime[0], splitTime[1]);
		var errorMsg = "Error: The 'End Date' is not allowed to come before 'Start Date'."

		if(endDate < startDate){
			$("#end_date").datepicker("setDate", "");
			document.getElementById("end_date").blur();
			document.getElementById("error_msg").innerHTML = errorMsg;
		}
		else
			document.getElementById("error_msg").innerHTML = "";
	}
</script>




		{{ Form::token() }}
		</form>
		</div>
	</div>
</div>