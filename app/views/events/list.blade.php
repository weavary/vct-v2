@extends('layouts.master')

@section('title')
<title>List Event</title>
@stop

@section('meta')
<meta name="Author" content="VCT" />
<meta name="Keywords" content="VCT, List, Event" />
<meta name="Description" content="List upcoming events in VCT" />

<meta property="og:image" content="{{url('images/index.jpg')}}" />
<meta property="og:url" content="{{{Request::url()}}}" />
<meta property="og:title" content="All Upcoming Events in VCT" />
<meta property="og:description" content="List upcoming events in VCT" />
@stop

@section('script')
{{HTML::style('css/bootstrap.css')}}
{{HTML::style('css/main.css')}}

{{HTML::script('js/jquery-1.11.1.js')}}
@stop

@section('content')
<div class="container body-margin">

	<ul class="nav nav-tabs" role="tablist">
  	<li class="active"><a href="{{URL::route('list-event')}}">List View</a></li>
  	<li><a href="{{URL::route('all-event')}}">All View</a></li>
	</ul>

<div class="container pull-left" style="max-width:720px">
	
	@foreach($events as $event)
	<div class="list-event">
	<a href="/event/{{$event->id}}">
	<div class="row">
		<div class="col-md-8 col-sm-8 col-xs-12">
		<img src="../../{{{$event->event_pic}}}" class="img-responsive col-xs-12" style="padding:0"/>
		</div>
		<div class="col-md-4 col-sm-4 col-xs-12">
			<h4>{{{$event->event_name}}}</h4>
			<p>
				<span class="glyphicon glyphicon-map-marker grey"></span> {{{$event->location}}}
				<br>
				<span class="glyphicon glyphicon-time green"></span> {{date("d M g:i:a",$event->start / 1000)}}
				<br>
				<span class="glyphicon glyphicon-time red"></span> {{date("d M g:i:a",$event->end / 1000)}}
			</p>
		</div>

	</div>
	</a>
	</div>
    @endforeach         

	</div>

</div>


{{HTML::script('js/bootstrap.js')}}
@stop
