@extends('layouts.master')

@section('script')
{{HTML::style('css/bootstrap.css')}}
{{HTML::style('css/main.css')}}

{{HTML::script('js/jquery-1.11.1.js')}}

<link href='http://fonts.googleapis.com/css?family=Sigmar+One' rel='stylesheet' type='text/css'>
<style>
h1 {
    font-family: "Impact", Charcoal, sans-serif;
    font-size: 5em;
}


h5{
	font-family: "Impact", Charcoal, sans-serif;
    font-size: 2em;
}
body{
	background: url('../images/background2.jpg');
	background-size: cover;
	background-attachment: fixed;
	background-repeat: no-repeat;
    background-position: center; 
}
</style>

@stop

@section('content')







	<div style="margin:12%">

	<center>
		@if(Session::has('pizza-success'))

		<div class="jumbotron" style="opacity:50">
			<button type='button' class='close' data-dismiss='alert'>x</button>
	    	<center><h2><span style="color:#5BC236">{{Session::get('pizza-success')}}</span></h2></center>
		    <center><p class="lead">You are cordially invited to our event.<br><span class="glyphicon glyphicon-calendar red"></span> 23 November 2014, 6pm-12pm.</p></center>
		</div>
		<img src="../images/thankyou.png" alt="Thank You" title="Thank You" class="img-responsive" />
		@else
		<img src="../images/pizza.png" alt="Pizza Kickstarter" title="Pizza Kickstarter" class="img-responsive" />
		@endif
		<br>
		<div id="button">
		<span class="glyphicon glyphicon-chevron-down glyphicon-down green"></span></div></center>
	</div>

	<div style="background-color:white;width:100%;box-shadow: 2px 2px 5px #cccccc;" id="myDiv">
		<div class="row" style="padding-top:40px; padding-bottom:40px; background-color:#eeeeee">
			<div class="col-md-6  col-md-offset-1 text-center">
				<div class="panel panel-default transparent" style="border-radius:0px;margin:10px;box-shadow: 2px 2px 5px #cccccc;">
					<div class="panel-body"style="background-color:white">

						<div class="embed-responsive embed-responsive-16by9">
							<iframe class="embed-responsive-item"  src="http://www.youtube.com/embed/l-HZPwTV2l0?modestbranding=1&showinfo=0&fs=0&controls=0&nologo=1" frameborder="0"></iframe>
						</div>
					</div>
					<div class="panel-footer" style="border-top:none;height:146px;width:100%;background-color:#eeeeee; background-repeat:no-repeat;background-size:100%">

						<div class="row">
							<div class="col-xs-6 col-md-3">
								<a href="#" class="thumbnail">
									<img data-src="holder.js/100%x180" alt="...">
								</a>
							</div>
  ...
						</div>
					</div>
				</div>
				
			</div>
			<div class="col-md-4 text-center">
				<div class="panel panel-default transparent" style="border-radius:0px;margin:10px;box-shadow: 2px 2px 5px #cccccc;">
					<div class="panel-body"style="background-color:white">
						<center>
							
							<strong style="font-size:1.8em">SUPPORT OUR TEAM</strong>
							<p style="font-size:1.5em">{{$count_down}} DAYS TO GO</p>
							<br>
							<button class="btn pizza-btn" id="in"type="submit"><h5>I'M IN</h5></button>
							<br><br>
							<strong style="font-size:1.8em">RM0</strong><br><p style="font-size:1.2em">Raise of RM20,000 stretch goal</p>
							<div class="progress progress-striped active">
								<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
									<span class="sr-only">0%</span>
								</div>
							</div>
						</center>
					</div>
					<div class="panel-footer" style="border-top:none;height:200px;width:100%;background-color:#eeeeee; background-repeat:no-repeat;background-size:100%">

						<br>
							Current Pledge
					</div>
				</div>
			</div>
		</div>
		<div class="row" id="raise" style="padding-top:40px; padding-bottom:40px">
			<div class="col-md-12 text-center" style="padding-top:20px; padding-bottom:40px">
				<strong style="font-size:1.5em">THE MORE YOU PLEDGE, THE BIGGER YOUR IMPACT</strong>
			</div>
			<div class="col-md-3  col-md-offset-2">
				<div class="list-group">
					<a id="5" class="list-group-item 5"><span class="pizza-font-size">RM5</span><br>Your name gets a SHOUTOUT!</a>
					<a id="10" class="list-group-item active 10"><span class="pizza-font-size">RM10</span><br>Get a free pizza specially baked by us!</a>
					<a id="20" class="list-group-item 20"><span class="pizza-font-size">RM20</span><br>Get a free Instax polaroid with us!</a>
					<a id="50" class="list-group-item 50"><span class="pizza-font-size">RM50</span><br>You are eligible to join us in our exciting pizza competition.</a>
					<a id="100" class="list-group-item 100"><span class="pizza-font-size">RM100</span><br>Receive an exclusive limited edition Pizza Kickstarter T-shirt!</a>
					<a id="250" class="list-group-item 250"><span class="pizza-font-size">RM250</span><br>Receive a personalized limited edition Pizza Kickstarter T-shirt!</a>
					<a id="500" class="list-group-item 500"><span class="pizza-font-size">RM500</span><br>We will wear creative, Funky DIY glasses with your name on it throughout the event day.</a>
					<a id="1000" class="list-group-item 1000"><span class="pizza-font-size">RM1000</span><br>You are entitled to sponsor prizes for the pizza competition.</a>
					<a id="x" class="list-group-item x"><span class="pizza-font-size">Pledge a custom amount</span></a>
				</div>
			</div>
			<div class="col-md-5 text-center">

        		<div id='pledge' class='pledges'>
        			<p style='font-size:1.5em; padding:40px 40px 0 40px'>At this pledge level you will get a free pizza specially baked by us!</p>
        			<p style='padding:0 40px 20px 40px'>We will bake the pizza on the event day.</p>
        			<center><img src='../images/tee-1.png' alt='Pizza Kickstarter RM10' title='Pizza Kickstarter RM10' class='img-responsive' /></center>
        			<a href='1/payment=10' class='btn pizza-btn' style='margin-top:40px' type='submit'><h5>PICK THIS PLEDGE</h5></a>
        		</div>



				<div class="xa hide-pizza">
					<div id='pledge'>
						<p style='font-size:1.5em; padding:40px'>Depending on how much you pledge, you can get different rewards</p>
						<center>
							<input type='text' class='form-control' style='width:200px' id='custom' placeholder='Enter Custom Amount'>
						</center>
						<a id='clickhere'class='btn pizza-btn' style='margin-top:40px'><h5>PICK THIS PLEDGE</h5></a>
					</div>
				</div>
			
			</div>
		</div>

		</div>
		<div class="row" style="padding-top:40px; padding-bottom:40px; background-color:#eeeeee">
			<div class="col-md-12 text-center" style="padding-top:20px; padding-bottom:40px">
				<strong style="font-size:1.5em">HOW PIZZA KICKSTARTER WORKS</strong>
			</div>

		</div>




	</div>





<script>
$("#button").click(function() {
    $('html, body').animate({
        scrollTop: $("#myDiv").offset().top
    }, 2000);
});
$('#pledge').click(function(){

	$( "#pick" ).append( "<p>Test</p>" );

});
$("#in").click(function() {
    $('html, body').animate({
        scrollTop: $("#raise").offset().top
    }, 2000);
});


$('.5').click(function(){

	$( ".list-group-item" ).removeClass("active")
	$( ".5" ).addClass( "active" );
	$('.xa').hide();

    $( "#pledge" ).html("<div id='pledge' class='pledges'><p style='font-size:1.5em; padding:40px 40px 0 40px'>At this pledge level your name gets a SHOUTOUT!</p><p style='padding:0 40px 20px 40px'>We will shoutout your name while we baking.</p><center><img src='../images/shoutout.png' width='200' alt='Pizza Kickstarter RM5' title='Pizza Kickstarter RM5' class='img-responsive' /></center><a href='1/payment=5' class='btn pizza-btn' style='margin-top:40px' type='submit'><h5>PICK THIS PLEDGE</h5></a></div>" );

    $('html, body').animate({
        scrollTop: $("#pledge").offset().top
    }, 1000);
});
$('.10').click(function(){
	$( ".list-group-item" ).removeClass("active")
	$( ".10" ).addClass( "active" );
	$('.xa').hide();

    $("#pledge").replaceWith(function(){
        return $("<div id='pledge' class='pledges'><p style='font-size:1.5em; padding:40px 40px 0 40px'>At this pledge level you will get a free pizza specially baked by us!</p><p style='padding:0 40px 20px 40px'>We will bake the pizza on the event day.</p><center><img src='../images/tee-1.png' alt='Pizza Kickstarter RM10' title='Pizza Kickstarter RM10' class='img-responsive' /></center><a href='1/payment=10' class='btn pizza-btn' style='margin-top:40px' type='submit'><h5>PICK THIS PLEDGE</h5></a></div>");
    });

    $('html, body').animate({
        scrollTop: $("#pledge").offset().top
    }, 1000);
});
$('.20').click(function(){
	$( ".list-group-item" ).removeClass("active")
	$( ".20" ).addClass( "active" );
	$('.xa').hide();

    $("#pledge").replaceWith(function(){
        return $("<div id='pledge' class='pledges'><p style='font-size:1.5em; padding:40px 40px 0 40px'>At this pledge level you will get a free Instax polaroid with us!</p><p style='padding:0 40px 20px 40px'>Take an Instax with us on the event day.</p><center><img src='../images/instax.png' alt='Pizza Kickstarter RM20' title='Pizza Kickstarter RM20' class='img-responsive' /></center><a href='1/payment=20' class='btn pizza-btn' style='margin-top:40px' type='submit'><h5>PICK THIS PLEDGE</h5></a></div>");
    });

    $('html, body').animate({
        scrollTop: $("#pledge").offset().top
    }, 1000);
});
$('.50').click(function(){
	$( ".list-group-item" ).removeClass("active")
	$( ".50" ).addClass( "active" );
	$('.xa').hide();

    $("#pledge").replaceWith(function(){
        return $("<div id='pledge' class='pledges'><p style='font-size:1.5em; padding:40px 40px 0 40px'>At this pledge level you are eligible to join us in our exciting pizza competition.</p><p style='padding:0 40px 20px 40px'>Take part in our fun and exciting pizza competition on event the day.</p><center><img src='../images/tee-1.png' alt='Pizza Kickstarter RM50' title='Pizza Kickstarter RM50' class='img-responsive' /></center><a href='1/payment=50' class='btn pizza-btn' style='margin-top:40px' type='submit'><h5>PICK THIS PLEDGE</h5></a></div>");
    });

    $('html, body').animate({
        scrollTop: $("#pledge").offset().top
    }, 1000);
});
$('.100').click(function(){
	$( ".list-group-item" ).removeClass("active")
	$( ".100" ).addClass( "active" );
	$('.xa').hide();

    $("#pledge").replaceWith(function(){
        return $("<div id='pledge' class='pledges'><p style='font-size:1.5em; padding:40px'>At this pledge level you will receive an exlusive limited edition Pizza Kickstarter T-shirt!</p><center><img src='../images/tee-1.png' alt='Pizza Kickstarter RM100' title='Pizza Kickstarter RM100' class='img-responsive' /></center><a href='1/payment=100' class='btn pizza-btn' style='margin-top:40px' type='submit'><h5>PICK THIS PLEDGE</h5></a></div>");
    });

    $('html, body').animate({
        scrollTop: $("#pledge").offset().top
    }, 1000);
});
$('.250').click(function(){
	$( ".list-group-item" ).removeClass("active")
	$( ".250" ).addClass( "active" );
	$('.xa').hide();

    $("#pledge").replaceWith(function(){
        return $("<div id='pledge' class='pledges'><p style='font-size:1.5em; padding:40px'>At this pledge level you will receive a personalized limited edition Pizza Kickstarter T-shirt!</p><center><img src='../images/tee-2.png' alt='Pizza Kickstarter RM250' title='Pizza Kickstarter RM250' class='img-responsive' /></center><a href='1/payment=250' class='btn pizza-btn' style='margin-top:40px' type='submit'><h5>PICK THIS PLEDGE</h5></a></div>");
    });

    $('html, body').animate({
        scrollTop: $("#pledge").offset().top
    }, 1000);
});
$('.500').click(function(){
	$( ".list-group-item" ).removeClass("active")
	$( ".500" ).addClass( "active" );
	$('.xa').hide();

    $("#pledge").replaceWith(function(){
        return $("<div id='pledge' class='pledges'><p style='font-size:1.5em; padding:40px'>At this pledge level, we will wear creative, Funky DIY glasses with your name on it throughout the event day.</p><center><img src='../images/tee-2.png' alt='Pizza Kickstarter RM500' title='Pizza Kickstarter RM500' class='img-responsive' /></center><a href='1/payment=500' class='btn pizza-btn' style='margin-top:40px' type='submit'><h5>PICK THIS PLEDGE</h5></a></div>");
    });

    $('html, body').animate({
        scrollTop: $("#pledge").offset().top
    }, 1000);
});
$('.1000').click(function(){
	$( ".list-group-item" ).removeClass("active")
	$( ".1000" ).addClass( "active" );
	$('.xa').hide();

    $("#pledge").replaceWith(function(){
        return $("<div id='pledge' class='pledges'><p style='font-size:1.5em; padding:40px'>At this pledge level you are entitled to sponsor prizes for the pizza competition.</p><center><img src='../images/prize.jpg' alt='Pizza Kickstarter RM1000' title='Pizza Kickstarter RM1000' class='img-responsive' /></center><a href='1/payment=1000' class='btn pizza-btn' style='margin-top:40px' type='submit'><h5>PICK THIS PLEDGE</h5></a></div>");
    });

    $('html, body').animate({
        scrollTop: $("#pledge").offset().top
    }, 1000);

});
$('.x').click(function(){
	$( ".list-group-item" ).removeClass("active")
	$( ".x" ).addClass( "active" );
	$('.pledges').hide(); 
	$('.xa').show(); 



    $('html, body').animate({
        scrollTop: $(".hide-pizza").offset().top
    }, 1000);
});

$("#clickhere").click(function(){
	var x = document.getElementById("custom").value.trim();
	if(x === ""){
		alert("Custom amount cannot be empty");
		return false;
	}
	if (!isNaN(x) && x >= 5){
		window.location.assign('1/payment='+x);
	}else if(x < 5){
		alert("Value must be more than 5");
		return false;
	}
	else{
		alert("Value must be valid number");
		return false;
	}
});




</script>

{{HTML::script('js/bootstrap.js')}}
@stop
