@extends('layouts.master')

@section('title')
<title>{{{$event->event_name}}}</title>
@stop

@section('meta')
<meta name="Author" content="VCT" />
<meta name="Keywords" content="{{{$event->desc}}}" />
<meta name="Description" content="{{{$event->desc}}}" />

<meta property="og:image" content="{{url($event->event_pic)}}" />
<meta property="og:url" content="{{{Request::url()}}}" />
<meta property="og:title" content="{{{$event->event_name}}}" />
<meta property="og:description" content="{{{$event->event_desc}}}" />
@stop


@section('script')
{{HTML::style('css/bootstrap.css')}}
{{HTML::style('css/main.css')}}
{{HTML::style('css/jquery-ui.css')}}

{{HTML::script('js/jquery-1.11.1.js')}}
@stop

@section('content')
<div class="container body-margin">
@if(!empty($admin))
<ul class="nav nav-tabs" role="tablist">
  <li><a href="{{{url('event/'.$event->id)}}}">{{{Str::limit($event->event_name, 15)}}}</a></li>
  <li class="active"><a href="{{{$url}}}">Settings</a></li>

</ul>
<div class="container pull-left" style="margin-top:10px;max-width:720px">
    	<form class="form-edit-site" role="form" action="{{URL::route('edit-event-post', array('id' => $event->id))}}" onsubmit="return validateForm()" method="post">
        	<h2 class="form-login-heading">Update Event</h2>
        	@if($errors->all())
		    <div class="bs-example">
		        <div class="alert alert-danger">
		        	<a href="#" class="close" data-dismiss="alert">&times;</a>
		            <strong>Errors!</strong> Please check the errors.<br>
		        </div>
		    </div>
			@endif

			
				<div class="form-group margin-edit-event">
					<div class="input-group">
		    			<div class="input-group-addon">Event Name</div>
        				@if($errors->has('event_name'))
        				<input type="text" class="form-control" id="event_name" name="event_name" placeholder="Event Name" autofocus {{ (Input::old('event_name')) ? ' value="' . e(Input::old('event_name')) . '"' : '' }}>
		    			</div>
						</div>
						<p class="text-danger">{{$errors->first('event_name')}}</p>
						@else
						<input type="text" class="form-control" id="event_name" name="event_name" value="{{{$event->event_name}}}"{{ (Input::old('event_name')) ? ' value="' . e(Input::old('event_name')) . '"' : '' }}>
					</div>
				</div>
				@endif
				<div class="form-group margin-edit-event">
		    		<div class="input-group">
		    			<div class="input-group-addon">Details</div>
		    			<textarea class="form-control" rows="4" id="event_desc" name="event_desc">{{{$event->event_desc}}}</textarea>
					</div>
				</div>

				<div class="form-group margin-edit-event">
					<div class="input-group">
		    			<div class="input-group-addon">Location</div>
						<input type="text" class="form-control" id="location" name="location" value="{{{$event->location}}}">
					</div>
				</div>

				<div class="form-group margin-edit-event">
					<div class="input-group">
						<div class="input-group-addon">Start Date</div>
				        <input type="text" onChange="validate()" id="start_date" class="form-control datepicker" name="start_date" id="lolz" placeholder="dd/mm/yyyy" value="{{date("d/m/Y",$event->start / 1000)}}">
					</div>
					
					<div class="input-group start_time">
				        <div class="input-group-addon">Start Time</div>
				        <select class="form-control" onChange="validate()" id="start_time" name="start_time" style="border-radius:0;-webkit-appearance: none">
				        	<option value="00.00">12.00am</option>
				        	<option value="00.30">12.30am</option>
				        	<option value="01.00">1.00am</option>
							<option value="01.30">1.30am</option>
							<option value="02.00">2.00am</option>
							<option value="02.30">2.30am</option>
							<option value="03.00">3.00am</option>
							<option value="03.30">3.30am</option>
							<option value="04.00">4.00am</option>
							<option value="04.30">4.30am</option>
							<option value="05.00">5.00am</option>
							<option value="05.30">5.30am</option>
							<option value="06.00">6.00am</option>
							<option value="06.30">6.30am</option>
							<option value="07.00">7.00am</option>
							<option value="07.30">7.30am</option>
							<option value="08.00">8.00am</option>
							<option value="08.30">8.30am</option>
							<option value="09.00">9.00am</option>
							<option value="09.30">9.30am</option>
							<option value="10.00">10.00am</option>
							<option value="10.30">10.30am</option>
							<option value="11.00">11.00am</option>
							<option value="11.30">11.30am</option>
							<option value="12.00">12.00pm</option>
							<option value="12.30">12.30pm</option>
							<option value="13.00">1.00pm</option>
							<option value="13.30">1.30pm</option>
							<option value="14.00">2.00pm</option>
							<option value="14.30">2.30pm</option>
							<option value="15.00">3.00pm</option>
							<option value="15.30">3.30pm</option>
							<option value="16.00">4.00pm</option>
							<option value="16.30">4.30pm</option>
							<option value="17.00">5.00pm</option>
							<option value="17.30">5.30pm</option>
							<option value="18.00">6.00pm</option>
							<option value="18.30">6.30pm</option>
							<option value="19.00">7.00pm</option>
							<option value="19.30">7.30pm</option>
							<option value="20.00">8.00pm</option>
							<option value="20.30">8.30pm</option>
							<option value="21.00">9.00pm</option>
							<option value="21.30">9.30pm</option>
							<option value="22.00">10.00pm</option>
							<option value="22.30">10.30pm</option>
							<option value="23.00">11.00pm</option>
							<option value="23.30">11.30pm</option>

						</select>
					</div>
					<div class="input-group">
						<div class="input-group-addon">End Date</div>
				        <input type="text" onChange="validate()" class="form-control datepicker" id="end_date" name="end_date" placeholder="dd/mm/yyyy" value="{{date("d/m/Y",$event->end / 1000)}}">
					</div>
					<div class="input-group end_time">
				        <div class="input-group-addon">End Time</div>
				        <select class="form-control" onChange="validate()" name="end_time"  id="end_time" style="border-radius:0;-webkit-appearance: none">
				        	<option value="00.00">12.00am</option>
				        	<option value="00.30">12.30am</option>
				        	<option value="01.00">1.00am</option>
							<option value="01.30">1.30am</option>
							<option value="02.00">2.00am</option>
							<option value="02.30">2.30am</option>
							<option value="03.00">3.00am</option>
							<option value="03.30">3.30am</option>
							<option value="04.00">4.00am</option>
							<option value="04.30">4.30am</option>
							<option value="05.00">5.00am</option>
							<option value="05.30">5.30am</option>
							<option value="06.00">6.00am</option>
							<option value="06.30">6.30am</option>
							<option value="07.00">7.00am</option>
							<option value="07.30">7.30am</option>
							<option value="08.00">8.00am</option>
							<option value="08.30">8.30am</option>
							<option value="09.00">9.00am</option>
							<option value="09.30">9.30am</option>
							<option value="10.00">10.00am</option>
							<option value="10.30">10.30am</option>
							<option value="11.00">11.00am</option>
							<option value="11.30">11.30am</option>
							<option value="12.00">12.00pm</option>
							<option value="12.30">12.30pm</option>
							<option value="13.00">1.00pm</option>
							<option value="13.30">1.30pm</option>
							<option value="14.00">2.00pm</option>
							<option value="14.30">2.30pm</option>
							<option value="15.00">3.00pm</option>
							<option value="15.30">3.30pm</option>
							<option value="16.00">4.00pm</option>
							<option value="16.30">4.30pm</option>
							<option value="17.00">5.00pm</option>
							<option value="17.30">5.30pm</option>
							<option value="18.00">6.00pm</option>
							<option value="18.30">6.30pm</option>
							<option value="19.00">7.00pm</option>
							<option value="19.30">7.30pm</option>
							<option value="20.00">8.00pm</option>
							<option value="20.30">8.30pm</option>
							<option value="21.00">9.00pm</option>
							<option value="21.30">9.30pm</option>
							<option value="22.00">10.00pm</option>
							<option value="22.30">10.30pm</option>
							<option value="23.00">11.00pm</option>
							<option value="23.30">11.30pm</option>

						</select>
					</div>

				</div>
		
					
{{HTML::script('js/jquery-ui.js')}}
<script>

$("div.start_time select").val("{{date("H.i",$event->start / 1000)}}");
$("div.end_time select").val("{{date("H.i",$event->end / 1000)}}");
$( ".datepicker" ).datepicker({
	dateFormat: 'dd/mm/yy',
	inline: true
});
$(".datepicker").each(function() {    
    $(this).datepicker('setDate', $(this).val());
});


// Hover states on the static widgets
$( "#dialog-link, #icons li" ).hover(
  function() {
    $( this ).addClass( "ui-state-hover" );
  },
  function() {
    $( this ).removeClass( "ui-state-hover" );
  }
);
</script> 

<script>
	function validateForm(){
		var event_name = document.getElementById("event_name").value;
		var event_desc = document.getElementById("event_desc").value;
		var location = document.getElementById("location").value;
		var start_date = document.getElementById("start_date").value;

		if(event_name === ""){
			alert("Event name cannot be empty");
			return false;
		}
		if(event_desc === ""){
			alert("Details cannot be empty");
			return false;
		}
		if(location === ""){
			alert("Location cannot be empty");
			return false;
		}
		if(start_date === ""){
			alert("Start date cannot be empty");
			return false;
		}
	}
</script>

<script>
	function validate(){
		var splitDate = document.getElementById("start_date").value.split("/");
		var splitTime = document.getElementById("start_time").value.split(".");
		var startDate = new Date(splitDate[2], splitDate[1] - 1, splitDate[0], splitTime[0], splitTime[1]);
		splitDate = document.getElementById("end_date").value.split("/");
		splitTime = document.getElementById("end_time").value.split(".");
		var endDate = new Date(splitDate[2], splitDate[1] - 1, splitDate[0], splitTime[0], splitTime[1]);
		var errorMsg = "Error: The 'End Date' is not allowed to come before 'Start Date'."

		if(endDate < startDate){
			$("#end_date").datepicker("setDate", "");
			document.getElementById("end_date").blur();
			document.getElementById("error_msg").innerHTML = errorMsg;

		}
		else
			document.getElementById("error_msg").innerHTML = "";
	}
</script>
					
        			<button class="btn btn-primary btn-block" type="submit">Update</button>
        			{{ Form::token() }}
    			</form>
  		
	</div>

</div>    
@else
@include('layouts.error-page')
@endif
            
</div>
{{HTML::script('js/bootstrap.js')}}
@stop
