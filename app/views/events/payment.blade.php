@extends('layouts.master')

@section('script')
{{HTML::style('css/bootstrap.css')}}
{{HTML::style('css/main.css')}}

{{HTML::script('js/jquery-1.11.1.js')}}
@stop

@section('content')
<div class="container body-margin">
<center>
	<a href="/event/1/"><img src="../../images/pizza.png" alt="Pizza Kickstarter"  width="150" title="Pizza Kickstarter" class="img-responsive" /></a>
</center>
@if($amount < 5)
<br><br>
<center><p>This amount is currently not available, please click to return to select the pledge</p>
<a href="../../event/1"class="btn pizza-btn" id="in"type="submit"><h5>I'M IN</h5></a></center>

@else
<div class="row">
	<div class="col-md-6">
		<div class="row">

		@if($amount >= 5)
		<div class="col-md-12">
			<h3>You are eligble for this reward as below</h3>
		</div>
		<div class="col-md-3">
			<div class="thumbnail">
				<img src='../../images/shoutout.png' alt='Pizza Kickstarter RM5' title='Pizza Kickstarter RM5' class='img-responsive' />
				<div class="caption text-center">
				<p>Your name gets a SHOUTOUT!</p>
				</div>
			</div>
		</div>
		@endif
		@if($amount >= 10)
		<div class="col-md-3">
			<div class="thumbnail">
				<img src='../../images/tee-1.png' alt='Pizza Kickstarter RM10' title='Pizza Kickstarter RM10' class='img-responsive' />
				<div class="caption text-center">
				<p>Get a free pizza specially baked by us!</p>
				</div>
			</div>
		</div>
		@endif
		@if($amount >= 20)
		<div class="col-md-3">
			<div class="thumbnail">
				<img src='../../images/instax.png' alt='Pizza Kickstarter RM20' title='Pizza Kickstarter RM20' class='img-responsive' />
				<div class="caption text-center">
				<p>Get a free Instax polaroid with us!</p>
				</div>
			</div>
		</div>
		@endif
		@if($amount >= 50)
		<div class="col-md-3">
			<div class="thumbnail">
				<img src='../../images/tee-1.png' alt='Pizza Kickstarter RM50' title='Pizza Kickstarter RM50' class='img-responsive' />
				<div class="caption text-center">
				<p>You are eligible to join us in our exciting pizza competition.</p>
				</div>
			</div>
		</div>
		<div class="clearfix"></div>

		@endif
		@if($amount >= 100)
		<div class="col-md-3">
			<div class="thumbnail">
				<img src='../../images/tee-1.png' alt='Pizza Kickstarter RM100' title='Pizza Kickstarter RM100' class='img-responsive' />
				<div class="caption text-center">
				<p>Receive an exclusive limited edition Pizza Kickstarter T-shirt!</p>
				</div>
			</div>
		</div>
		@endif
		@if($amount >= 250)
		<div class="col-md-3">
			<div class="thumbnail">
				<img src='../../images/tee-2.png' alt='Pizza Kickstarter RM250' title='Pizza Kickstarter RM250' class='img-responsive' />
				<div class="caption text-center">
				<p>Receive a personalized limited edition Pizza Kickstarter T-shirt!</p>
				</div>
			</div>
		</div>

		@endif
		@if($amount >= 500)
		<div class="col-md-3">
			<div class="thumbnail">
				<img src='../../images/tee-1.png' alt='Pizza Kickstarter RM500' title='Pizza Kickstarter RM500' class='img-responsive' />
				<div class="caption text-center">
				<p>We will wear creative, Funny DIY glasses with your name on it throughout the event day.</p>
				</div>
			</div>
		</div>
		@endif
		@if($amount >= 1000)
		<div class="col-md-3">
			<div class="thumbnail">
				<img src='../../images/prize.jpg' alt='Pizza Kickstarter RM1000' title='Pizza Kickstarter RM1000' class='img-responsive' />
				<div class="caption text-center">
				<p>You are entitled to sponsor prizes for the pizza competition.</p>
				</div>
			</div>
		</div>
		@endif
		
		</div>
	</div>
	<div class="col-md-6">
		<form class="form-create-site" role="form" action="{{URL::route('payment-event-post', array('id' => $id, 'amount' => $amount))}}" method="post" enctype="multipart/form-data">
        <h2 class="form-create-site-heading text-center">Do the pledge</h2>
        <p>Please bank in to our founder's account<br>
        	<strong>Name</strong> : Yoong Keat Wei<br>
        	<strong>Maybank</strong> : 157054713794<br>
        	<strong>Phone</strong> : +6016 444 2411
        </p>
        <p><em>All the pledges will fund the operation of VCT. Rest assured that we will compile and list out all of your pledges as a form of accountability.</em></p>
        
        @if($errors->all())
            <div class="bs-example">
                <div class="alert alert-danger">
                    <a href="#" class="close" data-dismiss="alert">&times;</a>
                    <strong>Errors!</strong> Please check the errors.<br>
                </div>
            </div>
        @endif
        <input type="text" class="form-control" value="Pledge: RM{{$amount}}" readOnly="true" style="cursor:text; background-color:#39FF14;font-size:1.5em;height:80px">
        <input type="hidden"name="pledge" value="{{$amount}}">
        <input type="text" class="form-control" name="name" placeholder="Name" {{(Input::old('name')) ? ' value="' . e(Input::old('name')) . '"' : '' }}>
        @if($errors->has('name'))
		<p class="text-danger">{{$errors->first('name')}}</p>
		@endif
        <input type="text" class="form-control" name="phone" placeholder="Phone" {{(Input::old('phone')) ? ' value="' . e(Input::old('phone')) . '"' : '' }}>
        @if($errors->has('phone'))
		<p class="text-danger">{{$errors->first('phone')}}</p>
		@endif
        <input type="email" class="form-control" name="email" placeholder="Email" {{(Input::old('email')) ? ' value="' . e(Input::old('email')) . '"' : '' }}>
        @if($errors->has('email'))
		<p class="text-danger">{{$errors->first('email')}}</p>
		@endif
		@if($amount >= 10)
		<div class="input-group">
			<div class="input-group-addon">Vegeterian</div>
			<input type="checkbox" class="form-control" name="vege">
		</div>
		@endif
        @if($amount >= 100)
        <input type="text" class="form-control" name="address" placeholder="Address" {{(Input::old('address')) ? ' value="' . e(Input::old('address')) . '"' : '' }}>
        @if($errors->has('address'))
		<p class="text-danger">{{$errors->first('address')}}</p>
		@endif
        <input type="text" class="form-control" name="postcode" placeholder="Postcode" {{(Input::old('postcode')) ? ' value="' . e(Input::old('postcode')) . '"' : '' }}>
        @if($errors->has('postcode'))
		<p class="text-danger">{{$errors->first('postcode')}}</p>
		@endif
        <input type="text" class="form-control" name="city" placeholder="City" {{(Input::old('city')) ? ' value="' . e(Input::old('city')) . '"' : '' }}>
        @if($errors->has('city'))
		<p class="text-danger">{{$errors->first('city')}}</p>
		@endif
        <div class="input-group">
				<div class="input-group-addon" >T-shirt Size</div>
				<select class="form-control" name="tshirt" style="border-radius:0;-webkit-appearance: none">
		        	<option value="" disabled {{(Input::old('tshirt')) == '' ? 'selected' : '' }}>Select Size</option>
		        	<option value="S" {{(Input::old('tshirt')) == 'S' ? 'selected' : '' }}>S</option>
		        	<option value="M" {{(Input::old('tshirt')) == 'M' ? 'selected' : '' }}>M</option>
		        	<option value="L" {{(Input::old('tshirt')) == 'L' ? 'selected' : '' }}>L</option>
					<option value="XL" {{(Input::old('tshirt')) == 'XL' ? 'selected' : '' }}>XL</option>
					<option value="XXL" {{(Input::old('tshirt')) == 'XXL' ? 'selected' : '' }}>XXL</option>
			</select>
		</div>
		@if($errors->has('tshirt'))
		<p class="text-danger">{{$errors->first('tshirt')}}</p>
		@endif
		@endif

		<div class="input-group">
			<div class="input-group-addon" >Upload Receipt</div>
			<input type="file" class="form-control" name="receipt">
		</div>
		@if($errors->has('receipt'))
		<p class="text-danger">{{$errors->first('receipt')}}</p>
		@endif
        <button class="btn btn-primary btn-block" type="submit">Submit</button>
        {{ Form::token() }}
    </form>
	</div>




</div>
@endif
</div>


{{HTML::script('js/bootstrap.js')}}
@stop
