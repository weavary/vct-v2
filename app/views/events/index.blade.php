@extends('layouts.master')

@section('title')
<title>{{{$event->event_name}}}</title>
@stop

@section('meta')
<meta name="Author" content="VCT" />
<meta name="Keywords" content="{{{$event->desc}}}" />
<meta name="Description" content="{{{$event->desc}}}" />

<meta property="og:image" content="{{url($event->event_pic)}}" />
<meta property="og:url" content="{{{Request::url()}}}" />
<meta property="og:title" content="{{{$event->event_name}}}" />
<meta property="og:description" content="{{{$event->event_desc}}}" />
@stop

@section('script')
{{HTML::style('css/bootstrap.css')}}
{{HTML::style('css/main.css')}}


{{HTML::script('js/jquery-1.11.1.js')}}
@stop

@section('content')
<div class="container body-margin">
@if(!empty($admin))
<ul class="nav nav-tabs" role="tablist">
  <li class="active"><a href="{{{url('event/'.$event->id)}}}">{{{Str::limit($event->event_name, 15)}}}</a></li>
  <li><a href="{{{$url}}}/settings">Settings</a></li>
</ul>
@endif
<div class="container pull-left" style="max-width:720px">
	@if($errors->all())
		<div class="bs-example">
		    <div class="alert alert-danger">
		        <a href="#" class="close" data-dismiss="alert">&times;</a>
		        <strong>Errors!</strong> Please check the errors.<br>
		        @if($errors->has('image'))
				<p class="text-danger">{{$errors->first('image')}}</p>
				@endif
		    </div>
		</div>
	@endif
	<h3>{{{$event->event_name}}} <span style="font-size:12px">by <a href="{{{url('site/'.$site->site_name.'/'.$site->id)}}}">{{{$site->site_name}}}</a></span></h3>
	<div class="imagewrap">
	@if($event->event_pic !=NULL)
	<img src="../../{{{$event->event_pic}}}" class="img-responsive col-xs-12"/>
	@else
	<img src="../../images/index.jpg" class="img-responsive col-xs-12"/>
	@endif

	@if(!empty($admin))
	<form class="input-image-file" id="form" action="{{URL::route('upload-photo-event-post', array('id' => $event->id))}}" method="post"  enctype="multipart/form-data">

			<span class="glyphicon glyphicon-camera red glyphicon-file"><input type="file" name="image" id="file">
			</span>
			{{ Form::token() }}

			
		</form>

		<script>
			document.getElementById("file").onchange = function() {
    	document.getElementById("form").submit();
		};
		</script>
		@endif
	</div>
	<br>
		<div class="row">
			<div class="col-md-6 col-sm-6">
				<p>
				<strong>Details</strong><br>
				{{{$event->event_desc}}}
				</p>
			</div>
			<div class="col-md-6 col-sm-6">
				<div class="col-md-6 col-sm-6" style="padding-left:0">
					<p>
					<strong>Location</strong><br>
					<span class="glyphicon glyphicon-map-marker grey"></span> {{{$event->location}}}<br>
					</p>

					
				</div>
				<div class="col-md-6 col-sm-6">
				</div>

				<div class="col-md-12 col-sm-12" style="padding-left:0">
					<p>
					<strong>Start Time</strong><br>
					<span class="glyphicon glyphicon-time green"></span> {{date("d F Y g:i:a",$event->start / 1000)}}
					</p>

					<p>
					<strong>End Time</strong><br>
					<span class="glyphicon glyphicon-time red"></span> {{date("d F Y g:i:a",$event->end / 1000)}}
					</p>
				</div>
			
			</div>
		</div>

             

	</div>

</div>


{{HTML::script('js/bootstrap.js')}}
@stop
