@extends('layouts.master')

@section('title')
<title>All Event</title>
@stop

@section('meta')
<meta name="Author" content="VCT" />
<meta name="Keywords" content="VCT, All, View, Event" />
<meta name="Description" content="View all event in VCT" />

<meta property="og:image" content="{{url('images/index.jpg')}}" />
<meta property="og:url" content="{{{Request::url()}}}" />
<meta property="og:title" content="View All Event in VCT" />
<meta property="og:description" content="View all event in VCT" />
@stop


@section('script')
{{HTML::style('css/bootstrap.css')}}
{{HTML::style('css/main.css')}}
{{HTML::style('css/calendar.css')}}

{{HTML::script('js/jquery-1.11.1.js')}}

@stop

@section('content')

<div class="container body-margin">
	
	<ul class="nav nav-tabs" role="tablist">
  	<li><a href="{{URL::route('list-event')}}">List View</a></li>
  	<li class="active"><a href="{{URL::route('all-event')}}">All View</a></li>
	</ul>

	<div class="page-header" style="border:0">

			<div><h3></h3></div>
			<div class="btn-group">
				<button class="btn btn-primary" data-calendar-nav="prev" style="border-radius:0"><span class="glyphicon glyphicon-chevron-left"></span>Prev</button>
				<button class="btn btn-default" data-calendar-nav="today" style="border-radius:0">Today</button>
				<button class="btn btn-primary" data-calendar-nav="next" style="border-radius:0">Next <span class="glyphicon glyphicon-chevron-right"></span></button>
			</div>

		
	</div>

	<div class="row">
		<div class="col-md-9">
			<div id="calendar"></div>
		</div>
	</div>

	

	{{HTML::script('js/jquery-1.11.1.js')}}
	{{HTML::script('js/underscore-min.js')}}
	{{HTML::script('js/bootstrap.js')}}
	{{HTML::script('js/jstz.js')}}
	{{HTML::script('js/calendar.js')}}
	{{HTML::script('js/app.js')}}


	
</div>
@stop
