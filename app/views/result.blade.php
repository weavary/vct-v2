@extends('layouts.master')

@section('title')
<title>{{{$q}}} - VCT Search</title>
@stop

@section('meta')
<meta name="Author" content="VCT" />
<meta name="Keywords" content="search" />
<meta name="Description" content="{{{$q}}} - VCT Search" />

<meta property="og:image" content="{{url('images/index.jpg')}}" />
<meta property="og:url" content="{{{Request::url()}}}" />
<meta property="og:title" content="VCT Search" />
<meta property="og:description" content="{{{$q}}} - VCT Search" />
@stop

@section('script')
{{HTML::style('css/bootstrap.css')}}
{{HTML::style('css/main.css')}}

{{HTML::script('js/jquery-1.11.1.js')}}
@stop

@section('content')
<div class="container body-margin">

	<div class="container pull-left" style="max-width:720px">
	<h4>You are searching for <strong>{{{$q}}}</strong><br><span class="red">{{count($results)}}</span> @if(count($results) > 1 ) results @else result @endif shown </h4>
	
	@foreach($results as $result)
	<div>
	<h4><img src="../../{{$result->site_pic}}" style="width:50px"/>&nbsp;<a href="/site/{{{$result->site_name}}}/{{{$result->id}}} ">{{{$result->site_name}}}</a></h4>
	<p style="font-size:1em">{{{$result->about}}}</p>

	</div>
		<hr>
    @endforeach 

	</div>

</div>


{{HTML::script('js/bootstrap.js')}}
@stop
