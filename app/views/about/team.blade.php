@extends('layouts.master')

@section('title')
<title>VCT Team</title>
@stop

@section('meta')
<meta name="Author" content="VCT" />
<meta name="Keywords" content="VCT, Team" />
<meta name="Description" content="The team behind VCT" />

<meta property="og:image" content="{{url('images/vct-team.jpg')}}" />
<meta property="og:url" content="{{{Request::url()}}}" />
<meta property="og:title" content="VCT Team" />
<meta property="og:description" content="The team behind VCT" />
@stop


@section('script')
{{HTML::style('css/bootstrap.css')}}
{{HTML::style('css/main.css')}}

{{HTML::script('js/jquery-1.11.1.js')}}
@stop

@section('content')
<div class="container body-margin">


	<center><h2>VCT Team</h2></center>
	<center><img src="../images/vct-team.jpg" alt="VCT Team" title="VCT Team" class="img-responsive img-thumbnail" style="margin-top:10px" /></center>
</div>
{{HTML::script('js/bootstrap.js')}}
@stop