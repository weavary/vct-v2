@extends('layouts.master')

@section('title')
<title>
@if(($people->first_name || $people->last_name) != NULL)
{{{$people->first_name}}} {{{$people->last_name}}}
@else
Profile
@endif
</title>
@stop

@section('meta')

<meta property="og:url" content="{{{Request::url()}}}" />
@if(($people->first_name || $people->last_name) != NULL)
<meta name="Keywords" content="{{{$people->first_name}}} {{{$people->last_name}}}, Profile" />
<meta name="Description" content="{{{$people->first_name}}} {{{$people->last_name}}}'s Profile" />

<meta property="og:title" content="{{{$people->first_name}}} {{{$people->last_name}}} joined VCT" />
<meta property="og:description" content="{{{$people->first_name}}} {{{$people->last_name}}} is awesome!" />
@else
<meta name="Keywords" content="VCT, Profile" />
<meta name="Description" content="Profile" />

<meta property="og:title" content="Profile" />
<meta property="og:description" content="Profile" />
@endif
@if($people->people_pic !=NULL)
<meta property="og:image" content="{{url($people->people_pic)}}" />
@else
<meta property="og:image" content="{{url('images/profile.jpg')}}" />
@endif

@stop

@section('script')
{{HTML::style('css/bootstrap.css')}}
{{HTML::style('css/main.css')}}

{{HTML::script('js/jquery-1.11.1.js')}}
@stop

@section('content')
<script>
  window.fbAsyncInit = function() {
    FB.init({
      appId      : '247439458778573',
      xfbml      : true,
      version    : 'v2.2'
    });
  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "//connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));
</script>
<div class="container body-margin">
<p style="font-size:8em; color:#666666">Hello.</p>

@if(($people->first_name || $people->last_name) != NULL)
<h1>{{{$people->first_name}}} {{{$people->last_name}}} is awesome!</h1>
		@if($errors->all())
		<div class="bs-example">
		    <div class="alert alert-danger">
		        <a href="#" class="close" data-dismiss="alert">&times;</a>
		        <strong>Errors!</strong> Please check the errors.<br>
		        @if($errors->has('image'))
				<p class="text-danger">{{$errors->first('image')}}</p>
				@endif
		    </div>
		</div>
		@endif
	<div class="imagewrap">
		@if($people->people_pic !=NULL)
		<img src="../../{{$people->people_pic}}" style="max-width:200px" class="img-responsive col-xs-12"/>
		@else
		<img src="../../images/profile.jpg" class="img-responsive col-xs-12">
		@endif
		@if(Auth::check())
		@if(Auth::user()->id == $people->id)
		<form class="input-image-file" id="form" action="{{{URL::route('upload-photo-people-post', array('id' => $people->id))}}}" method="post"  enctype="multipart/form-data">

			<span class="glyphicon glyphicon-camera red glyphicon-file"><input type="file" name="image" id="file">
			</span>
			{{ Form::token() }}

			
		</form>

		<script>
			document.getElementById("file").onchange = function() {
    	document.getElementById("form").submit();
		};
		</script>
		@endif
		@endif
	</div>

@endif
<br>
<a href="{{{URL::route('settings')}}}">Edit Profile</a>
<br>
<div
  class="fb-like"
  data-share="true"
  data-width="450"
  data-show-faces="true">
</div>
</div>
{{HTML::script('js/bootstrap.js')}}
@stop
