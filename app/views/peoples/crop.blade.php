@extends('layouts.master')

@section('title')
<title>
@if(($people->first_name || $people->last_name) != NULL)
{{{$people->first_name}}} {{{$people->last_name}}}
@else
Profile
@endif
</title>
@stop

@section('meta')
<meta name="Author" content="VCT" />
<meta name="Keywords" content="VCT, Profile" />
<meta name="Description" content="Profile" />

<meta property="og:image" content="{{url('images/index.jpg')}}" />
<meta property="og:url" content="{{{Request::url()}}}" />
<meta property="og:title" content="Profile" />
<meta property="og:description" content="Profile" />
@stop

@section('script')
{{HTML::style('css/bootstrap.css')}}
{{HTML::style('css/main.css')}}
{{HTML::style('css/jquery-ui.css')}}
{{HTML::style('css/jquery.Jcrop.min.css')}}


{{HTML::script('js/jquery-1.11.1.js')}}
{{HTML::script('js/jquery.Jcrop.min.js')}}
@stop

@section('content')



<div class="container body-margin">
	@if(Auth::user()->id == $people->id && !empty(Session::get('image')))
	<div class="container pull-left" style="max-width:720px">

		@if($errors->all())
		<div class="bs-example">
		    <div class="alert alert-danger">
		        <a href="#" class="close" data-dismiss="alert">&times;</a>
		        <strong>Errors!</strong> Please check the errors.<br>
		        @if($errors->has('image'))
				<p class="text-danger">{{$errors->first('image')}}</p>
				@endif
		    </div>
		</div>
		@endif
		<h3>Crop Your Site Photo</h3>
		<div class="imagewrap">
		<img src="../../../photos/temp/{{{Session::get('image')}}}" id="cropimage">

		<form class="submit-image-file" id="" action="{{{URL::route('crop-photo-people-post', array('id' => $id))}}}" method="post" enctype="multipart/form-data">


			<input type="hidden" name="image" value="{{Session::get('image')}}">
			<input type="hidden" name="x" value="" id="x">
			<input type="hidden" name="y" value="" id="y">
			<input type="hidden" name="w" value="" id="w">
			<input type="hidden" name="h" value="" id="h">

			

			<span class="glyphicon glyphicon-ok grey glyphicon-submit glyphicon-ok-crop"><input type="submit" value="Save Changed">Save
			</span><br>
			<a href="{{{URL::previous() }}}" class="grey cancel" ><span class="glyphicon glyphicon-remove"></span>Cancel</a>
			{{ Form::token() }}

			
		</form>
	
		<script type="text/javascript">
			$(function(){
				$('#cropimage').Jcrop({
					onSelect:updateCoords,
					setSelect:[0,0,200,200],
		            aspectRatio: 1 / 1
				});
			});

			function updateCoords(c){
				$('#x').val(c.x);
				$('#y').val(c.y);
				$('#w').val(c.w);
				$('#h').val(c.h);
			};
		</script>


		
		
		</div>



	</div>
	@else
@include('layouts.error-page')
@endif
</div>





{{HTML::script('js/bootstrap.js')}}
@stop