@extends('layouts.master')

@section('title')
<title>Update Profile</title>
@stop

@section('meta')
<meta name="Author" content="VCT" />
<meta name="Keywords" content="Profile, Settings" />
<meta name="Description" content="Profile settings" />

<meta property="og:image" content="{{url('images/index.jpg')}}" />
<meta property="og:url" content="{{{Request::url()}}}" />
<meta property="og:title" content="Profile Settings" />
<meta property="og:description" content="Profile settings" />
@stop

@section('script')
{{HTML::style('css/bootstrap.css')}}
{{HTML::style('css/main.css')}}

{{HTML::script('js/jquery-1.11.1.js')}}
@stop

@section('content')
<div class="container body-margin">


	<ul class="nav nav-tabs" role="tablist">
  	<li class="active"><a href="{{URL::route('settings-profile')}}">Profile</a></li>
  	<li><a href="{{URL::route('settings-password')}}">Password</a></li>
  		
	</ul>
	<div class="container pull-left" style="max-width:720px">
		<form class="form-edit-profile" role="form" action="{{URL::route('settings-profile-post')}}" method="post">
        	<h2 class="form-edit-profile-heading">Update Profile</h2>
        	@if($errors->all())
		    <div class="bs-example">
		        <div class="alert alert-danger">
		        	<a href="#" class="close" data-dismiss="alert">&times;</a>
		            <strong>Errors!</strong> Please check the errors.<br>
		        </div>
		    </div>
			@endif

				<div class="form-group margin-edit-profile">
		    		<div class="input-group">
		    			<div class="input-group-addon">Email</div>
						<input type="text" class="form-control" name="last_name" value="{{{Auth::user()->email}}}"  disabled style="cursor:text; background-color:white">
					</div>
				</div>

        		<div class="form-group margin-edit-profile">
		    		<div class="input-group">
		    			<div class="input-group-addon">First Name</div>
        				@if($errors->has('first_name'))
        				<input type="text" class="form-control" name="first_name" placeholder="First Name" autofocus {{ (Input::old('first_name')) ? ' value="' . e(Input::old('first_name')) . '"' : '' }}>
		    			</div>
						</div>
						<p class="text-danger">{{$errors->first('first_name')}}</p>
						@else
						<input type="text" class="form-control" name="first_name" value="{{{Auth::user()->first_name}}}"{{ (Input::old('first_name')) ? ' value="' . e(Input::old('first_name')) . '"' : '' }}>
					</div>
				</div>
				@endif

				<div class="form-group margin-edit-profile">
		    		<div class="input-group">
		    			<div class="input-group-addon">Last Name</div>
        				@if($errors->has('last_name'))
        				<input type="text" class="form-control" name="last_name" placeholder="Last Name" autofocus {{ (Input::old('last_name')) ? ' value="' . e(Input::old('last_name')) . '"' : '' }}>
		    			</div>
						</div>
						<p class="text-danger">{{$errors->first('last_name')}}</p>
						@else
						<input type="text" class="form-control" name="last_name" value="{{{Auth::user()->last_name}}}"{{ (Input::old('last_name')) ? ' value="' . e(Input::old('last_name')) . '"' : '' }}>
					</div>
				</div>
				@endif


		

					

				
        			<button class="btn btn-primary btn-block" type="submit">Update</button>
        			{{ Form::token() }}
    			</form>

	



	</div>
</div>
{{HTML::script('js/bootstrap.js')}}
@stop