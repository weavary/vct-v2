@extends('layouts.master')

@section('title')
<title>Change Password</title>
@stop

@section('meta')
<meta name="Author" content="VCT" />
<meta name="Keywords" content="Change, Password" />
<meta name="Description" content="Change password" />

<meta property="og:image" content="{{url('images/index.jpg')}}" />
<meta property="og:url" content="{{{Request::url()}}}" />
<meta property="og:title" content="Change Password" />
<meta property="og:description" content="Change password" />
@stop

@section('script')
{{HTML::style('css/bootstrap.css')}}
{{HTML::style('css/main.css')}}

{{HTML::script('js/jquery-1.11.1.js')}}
@stop

@section('content')
<div class="container body-margin">


	<ul class="nav nav-tabs" role="tablist">
  	<li><a href="{{URL::route('settings-profile')}}">Profile</a></li>
  	<li class="active"><a href="{{URL::route('settings-password')}}">Password</a></li>
  		
	</ul>
<div class="container pull-left" style="max-width:720px">
	<form class="form-edit-password" role="form" action="{{URL::route('settings-password-post')}}" method="post">
        	<h2 class="form-edit-password-heading">Change Password</h2>
        	@if($errors->all())
		    <div class="bs-example">
		        <div class="alert alert-danger">
		        	<a href="#" class="close" data-dismiss="alert">&times;</a>
		            <strong>Errors!</strong> Please check the errors.<br>
		        </div>
		    </div>
			@endif
        		
        		<div class="form-group margin-edit-password">
		    		<div class="input-group">
		    			<div class="input-group-addon" style="min-width:124px">Current Password</div>
        				
        				<input type="password" class="form-control" name="current_password" autofocus>
		    		</div>
				</div>
				@if($errors->has('current_password'))
				<p class="text-danger">{{$errors->first('current_password')}}</p>
				@endif
				

				<div class="form-group margin-edit-password">
		    		<div class="input-group">
		    			<div class="input-group-addon" style="min-width:124px">New Password</div>
        				<input type="password" class="form-control" name="password">
		    		</div>
				</div>
				@if($errors->has('password'))
				<p class="text-danger">{{$errors->first('password')}}</p>
				@endif

				<div class="form-group margin-edit-password">
		    		<div class="input-group">
		    			<div class="input-group-addon">Confirm Password</div>
        				
        				<input type="password" class="form-control datepicker" name="confirm_password">
		    		</div>
				</div>
				@if($errors->has('confirm_password'))
				<p class="text-danger">{{$errors->first('confirm_password')}}</p>
				@endif

		

		

					

				
        			<button class="btn btn-primary btn-block" type="submit">Change</button>
        			{{ Form::token() }}
    			</form>
	



	</div>
</div>
{{HTML::script('js/bootstrap.js')}}
@stop