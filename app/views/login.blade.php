@extends('layouts.master')

@section('title')
<title>VCT</title>
@stop

@section('meta')
<meta name="Author" content="VCT" />
<meta name="Keywords" content="Login, VCT" />
<meta name="Description" content="Log in with VCT" />

<meta property="og:image" content="{{url('images/index.jpg')}}" />
<meta property="og:url" content="{{{Request::url()}}}" />
<meta property="og:title" content="Log In VCT" />
<meta property="og:description" content="Log in with VCT" />
@stop

@section('script')
{{HTML::style('css/bootstrap.css')}}
{{HTML::style('css/main.css')}}

{{HTML::script('js/jquery-1.11.1.js')}}
@stop

@section('content')
<div class="container body-margin">
	@if(Session::has('success-join'))
	<div class="jumbotron">
		<button type='button' class='close' data-dismiss='alert'>x</button>
    	<center><h2><span style="color:#5BC236">Congratulations!</span></h2></center>
	    <center><p class="lead">{{Session::get('success-join')}}</p></center>
	</div>
	@endif
	@if(Session::has('fail-join'))
	<div class="jumbotron">
		<button type='button' class='close' data-dismiss='alert'>x</button>
    	<center><h2><span style="color:#f3002d">Sorry!</span> =(</h2></center>
	    <center><p class="lead">{{Session::get('fail-join')}}</p></center>
	</div>
	@endif
			
	
	
	
    <form class="form-login" role="form" action="{{{URL::route('login-post')}}}" method="post">
        <h2 class="form-edit-event-heading text-center">Welcome!</h2>
        @if($errors->all())
	    <div class="bs-example">
			<div class="alert alert-danger">
			    <a href="#" class="close" data-dismiss="alert">&times;</a>
		        <strong>Errors!</strong> Please check the errors.<br>
		    </div>
		</div>
		@endif
		@if(Session::has('error-message'))
	    <div class="bs-example">
			<div class="alert alert-danger">
			    <a href="#" class="close" data-dismiss="alert">&times;</a>
		        <strong>Errors!</strong> {{Session::get('error-message')}}<br>
		    </div>
		</div>
		@endif
        <input type="email" class="form-control" name="email" placeholder="Email address" {{ (Input::old('email')) ? ' value="' . e(Input::old('email')) . '"' : '' }} autofocus>
        @if($errors->has('email'))
		<p class="text-danger">{{$errors->first('email')}}</p>
		@endif
        <input type="password" class="form-control" name="password" placeholder="Password">
        @if($errors->has('password'))
		<p class="text-danger">{{$errors->first('password')}}</p>
		@endif
        <button class="btn btn-lg btn-primary btn-block" type="submit">Login</button>
        {{ Form::token() }}
        <div class="row">
			<div class="col-md-6"><a href="{{URL::route('join')}}">No Account Yet?</a></div>
			<div class="col-md-6 text-right"><a href="{{URL::route('forgot')}}">Forgotten Password</a></div>
	    </div>
    </form>
    


      


</div>
{{HTML::script('js/bootstrap.js')}}
@stop
