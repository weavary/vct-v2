@extends('layouts.master')

@section('title')
<title>About Advertising on VCT</title>
@stop

@section('meta')
<meta name="Author" content="VCT" />
<meta name="Keywords" content="VCT,Advertising" />
<meta name="Description" content="About Advertising on VCT" />

<meta property="og:image" content="{{url('images/platinum.jpg')}}" />
<meta property="og:url" content="{{{Request::url()}}}" />
<meta property="og:title" content="VCT Advertising" />
<meta property="og:description" content="About Advertising on VCT" />
@stop



@section('script')
{{HTML::style('css/bootstrap.css')}}
{{HTML::style('css/main.css')}}

{{HTML::script('js/jquery-1.11.1.js')}}

<style>
body {
  position: relative;
}
</style>
@stop

@section('content')
<div class="container body-margin">
	<div class="row" style="margin:0;margin-top:20px">
		<div class="col-md-2 col-sm-5 col-xs-5 ads-nav ads-nav-plat">
			<h3>Platinum<span class="red">+</span></h3>
			<p>
			<span class="glyphicon glyphicon-ok green"></span> Slider/year<br>
			<span class="glyphicon glyphicon-ok green"></span> Search Result/year<br>
			<span class="glyphicon glyphicon-ok green"></span> Slider Design<br><span style="padding-left:15px">(Free up to 3 designs)</span><br>
			</p>

			<img src="../images/platinum.jpg" class="img-responsive"/><br>
			<img src="../images/silver.jpg" class="img-responsive" style="margin-bottom:100px"/><br>
			<span class="ads-btn" style="position:absolute; bottom: 20px;"><h5>RM1200</h5></span>
		</div>
		<div class="col-md-2 col-sm-5 col-xs-5 ads-nav ads-nav-plat">
			<h3>Platinum</h3>
			<p>
			<span class="glyphicon glyphicon-ok green"></span> Slider/year<br>
			<span class="glyphicon glyphicon-ok green"></span> Slider Design<br><span style="padding-left:15px">(Free 1 design)</span><br>
			</p>
			<img src="../images/platinum.jpg" class="img-responsive" style="margin-bottom:100px"/><br>
			<span class="ads-btn" style="position:absolute; bottom: 20px;"><h5>RM900</h5></span>
			
		</div>
		<div class="clearfix visible-sm visible-xs"></div>
		<div class="col-md-2 col-sm-5 col-xs-5 ads-nav ads-nav-gold">
			<h3>Gold<span class="red">+</span></h3>
			<p>
			<span class="glyphicon glyphicon-ok green"></span> Side Bar/year<br>
			<span class="glyphicon glyphicon-ok green"></span> Search Result/year<br>
			</p>
			<img src="../images/gold.jpg" class="img-responsive"/><br>
			<img src="../images/silver.jpg" class="img-responsive" style="margin-bottom:100px"/><br>
			<span class="ads-btn" style="position:absolute; bottom: 20px;"><h5>RM700</h5></span>
			
		</div>
		<div class="col-md-2 col-sm-5 col-xs-5 ads-nav ads-nav-gold">
			<h3>Gold</h3>
			<p>
			<span class="glyphicon glyphicon-ok green"></span> Side Bar/year<br>
			</p>
			<img src="../images/gold.jpg" class="img-responsive" style="margin-bottom:100px"/><br>
			<span class="ads-btn" style="position:absolute; bottom: 20px;"><h5>RM500</h5></span>
			
		</div>
		<div class="clearfix visible-sm visible-xs"></div>
		<div class="col-md-2 col-sm-10 col-xs-10 ads-nav ads-nav-silver">
			<h3>Silver</h3>
			<p>
			<span class="glyphicon glyphicon-ok green"></span> Search Result/year<br>
			<span class="glyphicon glyphicon-ok green"></span> Free 1 week Side Bar<br>
			</p>
			<img src="../images/silver.jpg" class="img-responsive" style="margin-bottom:100px"/><br>
			<span class="ads-btn" style="position:absolute; bottom: 20px;"><h5>RM200</h5></span>

		</div>
	</div>
	<div class="row">
   		<center><p style="margin-top:10px"><a href="{{URL::route('helps-feedback')}}">Contact Us</a> | email :: <a href="mailto:hello@vct.my" style="text-decoration:none">hello@vct.my</a> | phone :: <a href="tel:+60164442411" style="text-decoration:none">+6016 444 2411</a></p></center>
	</div>

	
	
</div>
{{HTML::script('js/bootstrap.js')}}
@stop