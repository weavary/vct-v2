@extends('layouts.master')

@section('title')
<title>Search Food, Drinks, Services in Kampar. All available choices in VCT</title>
@stop

@section('meta')
<meta name="Author" content="VCT" />
<meta name="Keywords" content="Search, VCT,food, drinks, beverage, transport, car, bicycle, motorcycle" />
<meta name="Description" content="Search Kampar's latest promotions, services and information" />

<meta property="og:image" content="{{url('images/index.jpg')}}" />
<meta property="og:url" content="{{{Request::url()}}}" />
<meta property="og:title" content="Search Food, Drinks, Services in Kampar. All available choices in VCT" />
<meta property="og:description" content="Search Kampar's latest promotions, services and information" />
@stop

@section('script')
{{HTML::style('css/bootstrap.css')}}
{{HTML::style('css/main.css')}}

{{HTML::script('js/jquery-1.11.1.js')}}

<script src="../js/jquery.carouFredSel-6.2.1.js" type="text/javascript"></script>

<script type="text/javascript">
      $(function() {
        $('#slider').carouFredSel({
          width: '100%',
          align: false,
          items: 3,
          items: {
            width: $('#wrapper').width() * 0.15,
            height: 405,
            visible: 1,
            minimum: 1
          },
          scroll: {
            items: 1,
            timeoutDuration : 5000,
            onBefore: function(data) {
      
              //  find current and next slide
              var currentSlide = $('.slide.active', this),
                nextSlide = data.items.visible,
                _width = $('#wrapper').width();
      
              //  resize currentslide to small version
              currentSlide.stop().animate({
                width: _width * 0.15
              });   
              currentSlide.removeClass( 'active' );
      
              //  hide current block
              data.items.old.add( data.items.visible ).find( '.slide-block' ).stop().fadeOut();         
      
              //  animate clicked slide to large size
              nextSlide.addClass( 'active' );
              nextSlide.stop().animate({
                width: _width * 0.6
              });           
            },
            onAfter: function(data) {
              //  show active slide block
              data.items.visible.last().find( '.slide-block' ).stop().fadeIn();
            }
          },
          onCreate: function(data){
      
            //  clone images for better sliding and insert them dynamacly in slider
            var newitems = $('.slide',this).clone( true ),
              _width = $('#wrapper').width();

            $(this).trigger( 'insertItem', [newitems, newitems.length, false] );
      
            //  show images 
            $('.slide', this).fadeIn();
            $('.slide:first-child', this).addClass( 'active' );
            $('.slide', this).width( _width * 0.15 );
      
            //  enlarge first slide
            $('.slide:first-child', this).animate({
              width: _width * 0.6
            });
      
            //  show first title block and hide the rest
            $(this).find( '.slide-block' ).hide();
            $(this).find( '.slide.active .slide-block' ).stop().fadeIn();
          }
        });
      
        //  Handle click events
        $('#slider').children().click(function() {
          $('#slider').trigger( 'slideTo', [this] );
        });
      
        //  Enable code below if you want to support browser resizing
        $(window).resize(function(){
      
          var slider = $('#slider'),
            _width = $('#wrapper').width();
      
          //  show images
          slider.find( '.slide' ).width( _width * 0.15 );
      
          //  enlarge first slide
          slider.find( '.slide.active' ).width( _width * 0.6 );
      
          //  update item width config
          slider.trigger( 'configuration', ['items.width', _width * 0.15] );
        });

      });
    </script>


@stop

@section('content')


<div class="container body-margin">

  <div id="wrapper" class="hidden-xs">
      <div id="slider">
  
        <div class="slide" style="background: url('../images/join.jpg'); background-repeat:no-repeat">
          <div class="slide-block">
            <a href="{{URL::route('join')}}"><h4>Join VCT</h4>
            <p>Publish your service and information in VCT for FREE!</p>
            </a>
          </div>
        </div>
    
        <div class="slide" style="background: url('../images/event.jpg');background-repeat:no-repeat">
          <div class="slide-block">
            <a href="{{URL::route('list-event')}}"><h4>Grab the Latest Promotion</h4>
            <p>Lots of promotions and events in town.</p>
            </a>
          </div>
        </div>
    
        <div class="slide" style="background: url('../images/promoted.jpg');background-repeat:no-repeat">
          <div class="slide-block">
            <a href="{{URL::route('search', array('keyword' => 'food'))}}"><h4>Promote Your Services</h4>
            <p>Reach your customer instantly.</p>
            </a>
          </div>
        </div>

        <div class="slide" style="background: url('../images/food.jpg'); background-repeat:no-repeat">
          <div class="slide-block">
            <a href="{{URL::route('ads')}}"><h4>What to Eat Today?</h4>
            <p>Have no idea what to eat today? Search the available choices in town.</p>
            </a>
          </div>
        </div>

        <div class="slide" style="background: url('../images/transport.jpg');background-repeat:no-repeat">
          <div class="slide-block">
            <a href="{{URL::route('search', array('keyword' => 'transport'))}}"><h4>Need a Ride?</h4>
            <p>Search with VCT</p>
            </a>
          </div>
        </div>

    
      </div>
    </div>

    <div id="carousel-example-generic" class="carousel slide visible-xs" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
    <li data-target="#carousel-example-generic" data-slide-to="3"></li>
    <li data-target="#carousel-example-generic" data-slide-to="4"></li>
  </ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner" role="listbox">

    <div class="item active">
      <img src="../images/join.jpg" alt="...">
            <a href="{{URL::route('join')}}">

      <div class="carousel-caption">
        Join VCT
      </div>
    </a>
    </div>
    <div class="item">
      <img src="../images/event.jpg" alt="...">
            <a href="{{URL::route('list-event')}}">

      <div class="carousel-caption">
        Grab the Latest Promotion
      </div>
    </a>
    </div>
    <div class="item">
      <img src="../images/promoted.jpg" alt="...">
                  <a href="{{URL::route('ads')}}">

      <div class="carousel-caption">
        Promote Your Services
      </div>
    </a>
    </div>
    <div class="item">
      <img src="../images/food.jpg" alt="...">
                  <a href="{{URL::route('search', array('keyword' => 'food'))}}">

      <div class="carousel-caption">
        What to Eat Today?
      </div>
    </a>
    </div>
    <div class="item">
      <img src="../images/transport.jpg" alt="...">
                  <a href="{{URL::route('search', array('keyword' => 'transport'))}}">

      <div class="carousel-caption">
        Need a Ride?
      </div>
    </a>
    </div>
  </div>

  <!-- Controls -->
  <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
  <br>
	<div class="row">
    <div class="col-md-3 col-sm-3 col-xs-12 visible-xs">
      <div class="text-center" style="background-color:#f3002d; height:40px; line-height: 40px;">
        Promoted
      </div>
      @foreach($promotes as $promote)
      <div class="col-xs-12" style="padding:0">
        <img src="../../{{$promote->site_pic}}" class="img-responsive"/>
      </div>
      <div class="col-xs-12" style="padding:10px 0">
        <a href="{{{url('site/'.$promote->site_name.'/'.$promote->id)}}}" style="text-decoration:none; text-align:center"><p>{{{$promote->site_name}}}</p></a>
        <p>{{{$promote->about}}}
        </p>
      </div>
      @endforeach
    </div>
		<div class="col-md-4 col-sm-4 col-xs-12">
			<div class="text-center" style="background-color:#f3002d; height:40px; line-height: 40px;">
				Today Special
			</div>
      @if(!$specials->isEmpty())
			@foreach($specials as $special)
			<div class="col-md-6 col-sm-6 col-xs-4" style="padding:0">
				<img src="../../{{$special->event_pic}}" class="img-responsive"/>
			</div>
			<div class="col-md-6 col-sm-6 hidden-xs">
				<a href="{{{url('event/'.$special->id)}}}" style="text-decoration:none"><h4>{{{Str::limit($special->event_name, 20)}}}</h4></a>
				<p>{{{Str::limit($special->event_desc, 50)}}}</p>
				<p><span class="glyphicon glyphicon-time green"></span>Today till {{date("d F",$special->end / 1000)}}</p>
			</div>
      <div class="col-xs-8 visible-xs" style="padding:1px">
        <a href="{{{url('event/'.$special->id)}}}" style="text-decoration:none"><span>{{{Str::limit($special->event_name, 15)}}}</span></a><br>
        <span>{{{Str::limit($special->event_desc, 50)}}}</span>
      </div>
      <div class="col-xs-12 visible-xs" style="padding:1px">
        
        <p><span class="glyphicon glyphicon-time green"></span>Today till {{date("d F",$special->end / 1000)}}</p>      
      
      </div>
			<div class="clearfix"></div>
			@endforeach
      @else
      No event today.
      @endif
			
		</div> 
		<div class="col-md-5 col-sm-5 col-xs-12">
			<div class="text-center" style="background-color:#f3002d; height:40px; line-height: 40px;">
				Recommendation
			</div>
      <div class="col-md-12 col-sm-12" style="padding:0">
        <img src="../../{{$recommend->site_pic}}" class="img-responsive"/>
      </div>
      <div class="col-md-12 col-sm-12" style="padding:10px 0">
        <a href="{{{url('site/'.$recommend->site_name.'/'.$recommend->id)}}}" style="text-decoration:none"><p>{{{$recommend->site_name}}}</p></a>
        <p>This little stall set up among the terrace houses of Wah Loong village offers one of the best assam laksa in the town of Kampar. It's fish-based
          sweet and spicy soup with an assortment of fried ingredients beckons to thongs of students everyday. Known simply as "Wah Loong Laksa", one can
          drop by specially on Mondays and Thursday for their infamous laksa and refreshing honeydew sago otherwise known as 'sai mai lo'. Be sure to check
          it out if you happen to be in town!
        </p>
      </div>

		</div> 
		<div class="col-md-3 col-sm-3 col-xs-12 hidden-xs">
			<div class="text-center" style="background-color:#f3002d; height:40px; line-height: 40px;">
				Promoted
			</div>
      @foreach($promotes as $promote)
      <div class="col-md-12 col-sm-12" style="padding:0">
        <img src="../../{{$promote->site_pic}}" class="img-responsive"/>
      </div>
      <div class="col-md-12 col-sm-12" style="padding:10px 0">
        <a href="{{{url('site/'.$promote->site_name.'/'.$promote->id)}}}" style="text-decoration:none"><p>{{{$promote->site_name}}}</p></a>
        <p>{{{$promote->about}}}
        </p>
      </div>
      @endforeach
		</div>
	</div>
</div>

{{HTML::script('js/bootstrap.js')}}
@stop
