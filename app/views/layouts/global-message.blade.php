@if(Session::has('global-success'))
<div class="bs-example">
	<div class="alert alert-success">
		<a href="#" class="close" data-dismiss="alert">&times;</a>
		<strong>Success!</strong> {{Session::get('global-success')}}<br>
	</div>
</div>
@endif
@if(Session::has('global-error'))
<div class="bs-example">
	<div class="alert alert-danger">
		<a href="#" class="close" data-dismiss="alert">&times;</a>
		<strong>Errors!</strong> {{Session::get('global-error')}}<br>
	</div>
</div>
@endif
