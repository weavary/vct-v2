<nav class="navbar navbar-default" role="navigation" style="z-index:10000">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="{{URL::route('home')}}" id="logo"></a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <form class="navbar-form navbar-left visible-xs" action="{{URL::route('search-post')}}" method="post">
        <div class="input-group" style="max-width:500px">
          <input type="text" class="form-control search-input" placeholder="Search" name="search">
          <div class="input-group-btn" >
            <button class="btn btn-default search-button"  type="submit"><i class="glyphicon glyphicon-search red"></i></button>
          </div>
        </div>
        {{ Form::token() }}
      </form>
      <ul class="nav navbar-nav">
        <!-- <li class="dropdown">
          <a href="" class="dropdown-toggle" data-toggle="dropdown">Categories <span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">

            <li><a href="{{URL::route('list-event')}}">List Events</a></li>
          </ul>
        </li> -->
        <li class="dropdown">
          <a href="" class="dropdown-toggle" data-toggle="dropdown">Events <span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">

            <li><a href="{{URL::route('list-event')}}">List Events</a></li>
            <li><a href="{{URL::route('all-event')}}">All Events</a></li>
          </ul>
        </li>


      </ul>
      <form class="navbar-form navbar-left hidden-xs" action="{{URL::route('search-post')}}" method="post">
        <div class="input-group" style="max-width:500px">
          <input type="text" class="form-control search-input" placeholder="Search" name="search">
          <div class="input-group-btn" >
            <button class="btn btn-default search-button"  type="submit"><i class="glyphicon glyphicon-search red"></i></button>
          </div>
        </div>
        {{ Form::token() }}
      </form>
      <ul class="nav navbar-nav navbar-right">
        @if(Auth::check())

        
        <li>{{HTML::link('people/id='.Auth::user()->id, 'Profile')}}</li>
        <li class="dropdown">
          <a href="" class="dropdown-toggle" data-toggle="dropdown">Manage Site <span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">

            @foreach($nav_site->sites as $site)
            <li><a href="{{{url('site/'.$site->site_name.'/'.$site->id)}}}">{{{Str::limit($site->site_name, 15)}}}</a></li>
            @endforeach
            @if(!empty($nav_site))
            <li class="divider"></li>
            @endif
            <li><a href="{{URL::route('create-site')}}">Create Site</a></li>
          </ul>
        </li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-cog"></span></a>
          <ul class="dropdown-menu" role="menu">
            
            <li><a href="{{{URL::route('helps')}}}">Help?</a></li>
            <li class="divider"></li>
            
            <li><a href="{{{URL::route('settings')}}}">Settings</a></li>
            <li><a href="{{{URL::route('logout')}}}">Logout</a></li>
          </ul>
        </li>
        @else
        <li class="button-outline"><a href="{{{URL::route('join')}}}" style="color:black">Join</a></li>
        <li><a href="{{{URL::route('login')}}}">Log In</a></li>
        @endif
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>

