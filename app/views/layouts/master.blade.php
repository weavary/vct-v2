<!DOCTYPE html>
<html lang="en">
	<head>
		@yield('title')
		<meta charset="utf-8">
    	<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		@yield('meta')

		@yield('script')

		<link rel="shortcut icon" type="image/x-icon" href="{{ asset('favicon.ico') }}"/>



		
	</head>
	<body>
 	@include('layouts.navigation')
 	<div class="container">
 	@include('layouts.global-message')
 	</div>
 	@yield('content')
 	@include('layouts.footer')
 	<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-52264358-1', 'auto');
  ga('send', 'pageview');

</script>
	</body>
</html>