@extends('layouts.master')

@section('title')
<title>VCT Policies</title>
@stop

@section('meta')
<meta name="Author" content="VCT" />
<meta name="Keywords" content="VCT, Policies" />
<meta name="Description" content="VCT policies" />

<meta property="og:image" content="{{url('images/index.jpg')}}" />
<meta property="og:url" content="{{{Request::url()}}}" />
<meta property="og:title" content="VCT Policies" />
<meta property="og:description" content="VCT Policies" />
@stop

@section('script')
{{HTML::style('css/bootstrap.css')}}
{{HTML::style('css/main.css')}}

{{HTML::script('js/jquery-1.11.1.js')}}
@stop

@section('content')
<div class="container body-margin">

	<p><strong>VCT Directory Terms of Service ("Agreement")</strong></p>

	<p><em>This Agreement was last modified on September 30, 2014.</em></p>
	<p>Please read these Terms of Service completely before using www.vct.my which is owned and operated by VCT Online. This Agreement documents the legally binding terms and conditions attached to the use of the Site at www.vct.my.
</p>
	<p>By using or accessing the Site in any way, viewing or browsing the Site, or adding your own content to the Site, you are agreeing to be bound by these Terms of Service.</p>
	<p>The term ‘VCT Online' or 'us' or 'we' refers to the owner of the website whose registered office is 2172B, Jalan Batu Karang, Taman Bandar Baru, 31900 Kampar, Perak, Malaysia. The term 'you' refers to the user or viewer of our website.</p>
	<p><ins>User Data</ins></p>
	<p>
We do keep track of your search and browsing preferences for the sole purpose of using the information to improve our products and services.</p>
	<p><ins>Intellectual Property</ins></p>
	<p>The Site and all of its original content are the sole property of VCT Online and are, as such, fully protected by the appropriate international copyright and other intellectual property rights laws.
</p>
	<p><ins>Termination</ins></p>
	<p>VCT Online reserves the right to terminate your access to the Site, without any advance notice.</p>
	<p><ins>Links to Other Websites</ins></p>
	<p>Our Site does contain a number of links to other websites and online resources that are not owned or controlled by VCT Online.</p>
	<p>
VCT Online has no control over, and therefore cannot assume responsibility for, the content or general practices of any of these third party sites and/or services. Therefore, we strongly advise you to read the entire terms and conditions and privacy policy of any site that you visit as a result of following a link that is posted on our site.
</p>
	<p><ins>Governing Law </ins></p>
	<p>This Agreement is governed in accordance with the laws of Malaysia.</p>
	<p><ins>Changes to This Agreement</ins></p>
	<p>VCT Online reserves the right to modify these Terms of Service at any time. We do so by posting and drawing attention to the updated terms on the Site. Your decision to continue to visit and make use of the Site after such changes have been made constitutes your formal acceptance of the new Terms of Service.</p>
	<hr>
	<p>Therefore, we ask that you check and review this Agreement for such changes on an occasional basis. Should you not agree to any provision of this Agreement or any changes we make to this Agreement, we ask and advise that you do not use or continue to access the VCT Directory site immediately.</p>
	<p><ins>Contact Us</ins></p>
	<p>If you have any questions about this Agreement, please feel free to <a href="{{URL::route('helps-feedback')}}">contact us</a>.</p>
</div>

{{HTML::script('js/bootstrap.js')}}
@stop