@extends('layouts.master')

@section('title')
<title>How It Works - VCT</title>
@stop

@section('meta')
<meta name="Author" content="VCT" />
<meta name="Keywords" content="VCT, How it works" />
<meta name="Description" content="How it works" />

<meta property="og:image" content="{{url('images/index.jpg')}}" />
<meta property="og:url" content="{{{Request::url()}}}" />
<meta property="og:title" content="How It Works" />
<meta property="og:description" content="How it works" />
@stop

@section('script')
{{HTML::style('css/bootstrap.css')}}
{{HTML::style('css/main.css')}}

{{HTML::script('js/jquery-1.11.1.js')}}
@stop

@section('content')
<div class="container body-margin">


	<ul class="nav nav-tabs" role="tablist">
  	<li class="active"><a href="{{URL::route('helps-howtos')}}">How It Works</a></li>
  	<li><a href="{{URL::route('helps-faq')}}">FAQ</a></li>
  	<li><a href="{{URL::route('helps-feedback')}}">Feedback</a></li>
	</ul>

	<h3>For Everyone</h3>
	<p>
		<h4><span class="glyphicon glyphicon-search red"></span> Search for Services</h4> with search engine by keyword
		<h4><span class="glyphicon glyphicon-calendar red"></span> View Latest Events and Promotion</h4> with list view and calendar view
	</p><br>

	<h3>For Organization</h3>
	<p>
		<h4><span class="glyphicon glyphicon-user red"></span> Create Account</h4> with your personal details
		<h4><span class="glyphicon glyphicon-globe red"></span> Create Site</h4> for your organization
		<h4><span class="glyphicon glyphicon-calendar red"></span> Post Events</h4> sales, contests, promotions etc.
	</p>
</div>
{{HTML::script('js/bootstrap.js')}}
@stop