@extends('layouts.master')

@section('title')
<title>FAQs - VCT</title>
@stop

@section('meta')
<meta name="Author" content="VCT" />
<meta name="Keywords" content="VCT, FAQ" />
<meta name="Description" content="Learn about VCT" />

<meta property="og:image" content="{{url('images/index.jpg')}}" />
<meta property="og:url" content="{{{Request::url()}}}" />
<meta property="og:title" content="FAQs" />
<meta property="og:description" content="Learn about VCT" />
@stop

@section('script')
{{HTML::style('css/bootstrap.css')}}
{{HTML::style('css/main.css')}}

{{HTML::script('js/jquery-1.11.1.js')}}
@stop

@section('content')
<div class="container body-margin">


	<ul class="nav nav-tabs" role="tablist">
  	<li><a href="{{URL::route('helps-howtos')}}">How It Works</a></li>
  	<li class="active"><a href="{{URL::route('helps-faq')}}">FAQ</a></li>
  	<li><a href="{{URL::route('helps-feedback')}}">Feedback</a></li>
	</ul>
	<h3>Basics</h3>

	<div class="panel-group" id="accordion">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h4 class="panel-title">
					<a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">How do I find events that are happening?</a>
				</h4>
			</div>
			<div id="collapseOne" class="panel-collapse collapse">
				<div class="panel-body">
					<p>You can find events that are currently happening at the front page, in the <strong>“Today’s Special”</strong> column. These current and upcoming events are represented in a neat list view and you can view them by going to the navigation bar at the top. To view, navigate to <strong>“Events > List Events”</strong>.</p>
					<p>You can also find all our past, present or even future events by navigating to <strong>“Events > All Events”</strong>. All these events will be represented in a calendar layout.</p>

				</div>
			</div>
		</div>
		<div class="panel panel-default">
			<div class="panel-heading">
				<h4 class="panel-title">
					<a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">How do I find past events?</a>
				</h4>
			</div>
			<div id="collapseTwo" class="panel-collapse collapse">
				<div class="panel-body">
					<p>You can find past events by navigating to <strong>“Events > All Events”</strong>. These events will be represented in a calendar layout.</p>
				</div>
			</div>
		</div>
		<div class="panel panel-default">
			<div class="panel-heading">
				<h4 class="panel-title">
					<a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">How do I list my business on VCT?</a>
				</h4>
			</div>
			<div id="collapseThree" class="panel-collapse collapse">
				<div class="panel-body">
					<p>Firstly, you will need to log in or register for an account if you do not have an existing one. Next, click <strong>“Manage Site > Create Site”</strong> to create a site for your business. Fill in the required details to continue and your business will now be listed on VCT!</p>
					<p>You can add on additional details such as your business logo, operating hours and features under the “Settings” tab of your business site, which can be accessed from <strong>“Manage Site > Your Business Name”</strong>.</p>
				</div>
			</div>
		</div>
		<div class="panel panel-default">
			<div class="panel-heading">
				<h4 class="panel-title">
					<a data-toggle="collapse" data-parent="#accordion" href="#collapseFour">How do I create events?</a>
				</h4>
			</div>
			<div id="collapseFour" class="panel-collapse collapse">
				<div class="panel-body">
					<p>To create an event, you will need to have had created at least one site. At your current site (which can be accessed via <strong>“Manage Site > Your Business Name”</strong>), navigate to the <strong>“Create Event”</strong> tab to create time- limited promotional events, sales, and special discounts for your site.</p>
				</div>
			</div>
		</div>
		<h3>Account</h3>
		<div class="panel panel-default">
			<div class="panel-heading">
				<h4 class="panel-title">
					<a data-toggle="collapse" data-parent="#accordion" href="#collapseFive">Why do I need to create an account?</a>
				</h4>
			</div>
			<div id="collapseFive" class="panel-collapse collapse">
				<div class="panel-body">
					<p>With an account, you will be able to create an online presence for your business on VCT and retain access to contents that you’ve created. The account will enable you to manage your events and sites.</p>
				</div>
			</div>
		</div>
		<div class="panel panel-default">
			<div class="panel-heading">
				<h4 class="panel-title">
					<a data-toggle="collapse" data-parent="#accordion" href="#collapseSix">How do I change my password?</a>
				</h4>
			</div>
			<div id="collapseSix" class="panel-collapse collapse">
				<div class="panel-body">
					<p>To change your account password, you will need to log in or register for an account if you do not already have one, and go to the top right side of the navigation bar and look for a gear icon. Click on the icon and select <strong>“Settings”</strong>. You will be able to change your password in the Settings page.</p>
				</div>
			</div>
		</div>		
		<div class="panel panel-default">
			<div class="panel-heading">
				<h4 class="panel-title">
					<a data-toggle="collapse" data-parent="#accordion" href="#collapseSeven">I forgot my password. How do I reset it?</a>
				</h4>
			</div>
			<div id="collapseSeven" class="panel-collapse collapse">
				<div class="panel-body">
					<p>In any event that you might have forgotten your password, you can recover your access to your account by clicking <strong>“Forgotten Password”</strong> at the Log in page. Just type in your account email and an email with instructions will be sent to your mailbox. A new password will be generated and sent to you.</p>
				</div>
			</div>
		</div>		
		<div class="panel panel-default">
			<div class="panel-heading">
				<h4 class="panel-title">
					<a data-toggle="collapse" data-parent="#accordion" href="#collapseEight">How do I manage my profile?</a>
				</h4>
			</div>
			<div id="collapseEight" class="panel-collapse collapse">
				<div class="panel-body">
					<p>To manage your personal profile, you will need to log in or register for an account if you do not already have one, and go to the top right of the navigation bar and look for a gear icon. Click on the icon and select <strong>“Settings”</strong>. You will be able to update your profile details and change your password in the Settings page.</p>
				</div>
			</div>
		</div>
		<h3>Security and Privacy</h3>
		<div class="panel panel-default">
			<div class="panel-heading">
				<h4 class="panel-title">
					<a data-toggle="collapse" data-parent="#accordion" href="#collapseNine">Who will have access to my personal profile?</a>
				</h4>
			</div>
			<div id="collapseNine" class="panel-collapse collapse">
				<div class="panel-body">
					<p>You alone have access to your personal profile. Only site profiles are public.</p>
				</div>
			</div>
		</div>		
		<div class="panel panel-default">
			<div class="panel-heading">
				<h4 class="panel-title">
					<a data-toggle="collapse" data-parent="#accordion" href="#collapseTen">Who will have access to my site’s profile?</a>
				</h4>
			</div>
			<div id="collapseTen" class="panel-collapse collapse">
				<div class="panel-body">
					<p>All visitors at VCT will be able to view your site and any events that are organised by your site.</p>
				</div>
			</div>
		</div>		

</div>
</div>
{{HTML::script('js/bootstrap.js')}}
@stop