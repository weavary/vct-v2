@extends('layouts.master')

@section('title')
<title>Help us to 5 stars</title>
@stop

@section('meta')
<meta name="Author" content="VCT" />
<meta name="Keywords" content="VCT, Feedback, Help us, 5 stars" />
<meta name="Description" content="Help us to 5stars" />

<meta property="og:image" content="{{url('images/index.jpg')}}" />
<meta property="og:url" content="{{{Request::url()}}}" />
<meta property="og:title" content="Feedback" />
<meta property="og:description" content="Help us to 5stars" />
@stop

@section('script')
{{HTML::style('css/bootstrap.css')}}
{{HTML::style('css/main.css')}}

{{HTML::script('js/jquery-1.11.1.js')}}
@stop

@section('content')
<div class="container body-margin">


	<ul class="nav nav-tabs" role="tablist">
  	<li><a href="{{URL::route('helps-howtos')}}">How It Works</a></li>
  	<li><a href="{{URL::route('helps-faq')}}">FAQ</a></li>
  	<li class="active"><a href="{{URL::route('helps-feedback')}}">Feedback</a></li>
	</ul>

	@if(Session::has('feedback-success'))

		<div class="jumbotron" style="opacity:50">
		    <center><img src="../images/star.png" title="VCT 5 Star" alt="VCT 5 Star"></center>
	    	<center><h2><span style="color:#5BC236">{{Session::get('feedback-success')}}</span></h2></center>
		    <center><p class="lead">Your feedback means alot to us.</p></center>
		</div>
		@else

	<form class="form-edit-site" role="form" action="{{URL::route('helps-feedback-post')}}" method="post">
        <h2 class="form-edit-site-heading">Help us to 5 stars</h2>
        @if($errors->all())
		<div class="bs-example">
	        <div class="alert alert-danger">
	        	<a href="#" class="close" data-dismiss="alert">&times;</a>
	            <strong>Errors!</strong> Please check the errors.<br>
	        </div>
	    </div>
		@endif
  		<input type="text" class="form-control" name="name" placeholder="Name" @if(Auth::check()) @if((Auth::user()->first_name && Auth::user()->last_name ) != NULL) value="{{{Auth::user()->first_name}}} {{{Auth::user()->last_name}}}" @endif @endif {{ (Input::old('name')) ? ' value="' . e(Input::old('name')) . '"' : '' }}>
  		@if($errors->has('name'))
		<p class="text-danger">{{$errors->first('name')}}</p>
		@endif
		<input type="text" class="form-control" name="email" placeholder="Email"  @if(Auth::check()) value="{{{Auth::user()->email}}}" @endif{{ (Input::old('email')) ? ' value="' . e(Input::old('email')) . '"' : '' }}>
		@if($errors->has('email'))
		<p class="text-danger">{{$errors->first('email')}}</p>
		@endif
		<div class="input-group">
			<div class="input-group-addon">Emotion State</div>
			<select class="form-control" name="emotional_state" style="border-radius:0;-webkit-appearance: none">
		        <option value="" disabled selected></option>
		        <option value="Excited">Excited</option>
		        <option value="Confused">Confused</option>
		        <option value="Worried">Worried</option>
				<option value="Upset">Upset</option>
				<option value="Panicked">Panicked</option>
				<option value="Angry">Angry</option>
			</select>
		</div>
		@if($errors->has('emotional_state'))
		<p class="text-danger">{{$errors->first('emotional_state')}}</p>
		@endif
		<textarea class="form-control" rows="5" name="message" placeholder="Tell us what you think"></textarea>
		@if($errors->has('message'))
		<p class="text-danger">{{$errors->first('message')}}</p>
		@endif
       	<button class="btn btn-primary btn-block" type="submit">Send</button>
   		{{ Form::token() }}
   		<center><p style="margin-top:10px">email :: <a href="mailto:hello@vct.my" style="text-decoration:none">hello@vct.my</a> | phone :: <a href="tel:+60164442411" style="text-decoration:none">+6016 444 2411</a></p></center>

	</form>
	@endif
</div>
{{HTML::script('js/bootstrap.js')}}
@stop