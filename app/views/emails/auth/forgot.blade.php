<!DOCTYPE html>

<html>
	<head>
		<title>VCT</title>
		<meta charset="utf-8">

		
	</head>
	<body style="background-color:#dddddd">

		<div style="width:100%; margin:auto;background-color:white; font-family: Helvetica, Arial, sans-serif">
			<table style="width:100%; text-align:center; border-spacing:0" border="0">
				<tr style="height:40px">
					<td style="padding-top:10px; padding-bottom:5px; background-color:#f3002d; vertical-align:middle">
						<a href="https://www.vct.my">
						<img src="https://www.vct.my/images/web-logo-white.png" alt="VCT Logo">
						</a>
					</td>
				</tr>
				<tr>
					<td>
						<p style="font-size: 20px;padding-top:10px; padding-bottom:5px">Great Day!</p>
						<p style="font-size: 14px;font-weight: 300;line-height: 1.4em">
						Your have requested your password to be reset.<br>
						A new password had generated for you.<br><br>
						Please recover your password by clicking the link below and sign in with your new password.
						</p>
					</td>
				</tr>
				<tr>
					<td style="height:60px">
						<p style="width:100px; height:16px; border-radius:5px; margin:0 auto; background-color:#5BC236; padding-top:10px; padding-bottom:10px;display:inline-block">
							<a href="{{$link}}" style="text-decoration:none; color:white; font-weight: 300">Recover</a>
						</p>
					</td>
				</tr>
				<tr>
					<td style="height:60px">
						<p style="border:1px solid #aaaaaa; width:400px; height:16px; padding-top:10px; padding-bottom:10px;table-layout: fixed; margin: 0 auto;display:inline-block">
						New Password : {{$password}}
						</p>
					</td>
				</tr>
				<tr>
					<td style="height:60px">
						<p style="margin-bottom: 17px;font-size: 12px;font-weight: 300;line-height: 1.4em">
						If you have not requested to change your password, simply ignore this email.<br>
						If you suspect that your account may be compromised please contact us.
						</p>
					</td>
				</tr>
				<tr>
					<td style="height:60px">
						<p style="margin-bottom: 17px;font-size: 14px;font-weight: 300;line-height: 1.4em">
						Happy Day!<br>
						The VCT Team
						</p>
					</td>
				</tr>
				<tr>
					<td style="height:20px; background-color:#f3002d; line-height:20px; font-size:12px">
						<a href="https://www.vct.my/helps/howtos" style="text-decoration:none;color:white">How It Works</a> | 
						<a href="https://www.vct.my/helps/feedback" style="text-decoration:none;color:white">Feedback</a>
					</td>
				</tr>
			</table>
		</div>
		
 
	
	</body>
</html>