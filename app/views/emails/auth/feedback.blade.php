<!DOCTYPE html>

<html>
	<head>
		<title>VCT</title>
		<meta charset="utf-8">

		
	</head>
	<body style="background-color:#dddddd">

		<div style="width:100%; margin:auto;background-color:white; font-family: Helvetica, Arial, sans-serif">
			<table style="width:100%; text-align:center; border-spacing:0" border="0">
				<tr style="height:40px">
					<td style="padding-top:10px; padding-bottom:5px; background-color:#f3002d; vertical-align:middle">
						<a href="https://www.vct.my">
						<img src="https://www.vct.my/images/web-logo-white.png" alt="VCT Logo">
						</a>
					</td>
				</tr>
				<tr>
					<td style="text-align:left;padding-left:20px">
						<p style="font-size: 20px;padding-top:10px; padding-bottom:5px">Thank You {{$name}} For Helping Us Improve</p>
						<p style="font-size: 14px;font-weight: 300;line-height: 1.4em">
						Emotional State: {{$emo}}</span><br>Feedback: {{$msg}}
						</p>
					</td>
				</tr>
				<tr>
					<td style="height:60px">
						<p style="margin-bottom: 17px;font-size: 14px;font-weight: 300;line-height: 1.4em">
						Happy Day!<br>
						The VCT Team
						</p>
					</td>
				</tr>
				<tr>
					<td style="height:20px; background-color:#f3002d; line-height:20px; font-size:12px">
						<a href="https://www.vct.my/helps/howtos" style="text-decoration:none;color:white">How It Works</a> | 
						<a href="https://www.vct.my/helps/feedback" style="text-decoration:none;color:white">Feedback</a>
					</td>
				</tr>
			</table>
		</div>
		
 
	
	</body>
</html>