@extends('layouts.master')

@section('title')
<title>VCT</title>
@stop

@section('meta')
<meta name="Author" content="VCT" />
<meta name="Keywords" content="Join, VCT" />
<meta name="Description" content="Join VCT" />

<meta property="og:image" content="{{url('images/index.jpg')}}" />
<meta property="og:url" content="{{{Request::url()}}}" />
<meta property="og:title" content="Join VCT" />
<meta property="og:description" content="Join VCT" />
@stop

@section('script')
{{HTML::style('css/bootstrap.css')}}
{{HTML::style('css/main.css')}}

{{HTML::script('js/jquery-1.11.1.js')}}
@stop

@section('content')
<div class="container body-margin">
    
    <form class="form-join" role="form" action="{{URL::route('join-post')}}" method="post">
        <h2 class="form-join-heading text-center">Create Your VCT Account</h2>
        @if($errors->all())
        <div class="bs-example">
            <div class="alert alert-danger">
                <a href="#" class="close" data-dismiss="alert">&times;</a>
                <strong>Errors!</strong> Please check the errors.<br>
            </div>
        </div>
        @endif
        <input type="email" class="form-control" name="email" placeholder="Email address" {{ (Input::old('email')) ? ' value="' . e(Input::old('email')) . '"' : '' }} autofocus>
        @if($errors->has('email'))
		<p class="text-danger">{{$errors->first('email')}}</p>
		@endif
        <input type="password" class="form-control" name="password" placeholder="Password">
        @if($errors->has('password'))
		<p class="text-danger">{{$errors->first('password')}}</p>
		@endif
        <button class="btn btn-lg btn-primary btn-block" type="submit">Register</button>
        {{ Form::token() }}
        <div class="row">
            <center><div class="col-md-12">Already on VCT? <a href="{{URL::route('login')}}">Log In</a></div></center>
        </div>
    </form>
</div>
{{HTML::script('js/bootstrap.js')}}
@stop
