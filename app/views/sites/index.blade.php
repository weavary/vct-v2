@extends('layouts.master')

@section('title')
<title>{{{$site->site_name}}}</title>
@stop

@section('meta')
<meta name="Author" content="VCT" />
<meta name="Keywords" content="{{{$site->about}}}" />
<meta name="Description" content="{{{$site->about}}}" />

<meta property="og:image" content="{{url($site->site_pic)}}" />
<meta property="og:url" content="{{{Request::url()}}}" />
<meta property="og:title" content="{{{$site->site_name}}}" />
<meta property="og:description" content="{{{$site->about}}}" />
@stop

@section('script')
{{HTML::style('css/bootstrap.css')}}
{{HTML::style('css/main.css')}}
{{HTML::style('css/jquery-ui.css')}}

{{HTML::script('js/jquery-1.11.1.js')}}

@if(($site->lat && $site->lng) != NULL)
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp"></script>
<script>
var map;
function initialize() {
    var myLatlng = new google.maps.LatLng({{$site->lat}}, {{$site->lng}});
  var mapOptions = {
    zoom: 15,
    center: myLatlng,
    disableDefaultUI: true,
    styles:[{
        featureType:"poi",
        elementType:"labels",
        stylers:[{
            visibility:"off"
        }]
    }]
  };
  map = new google.maps.Map(document.getElementById('map-canvas'),
      mapOptions);
        var marker = new google.maps.Marker({
      position: myLatlng,
      map: map,
      title: '{{{$site->site_name}}}'
  });
}
google.maps.event.addDomListener(window, 'load', initialize);
</script>
@endif

@stop

@section('content')
<div class="container body-margin">

	@if(!empty($admin))
	<ul class="nav nav-tabs" role="tablist">
  	<li class="active"><a href="{{{url('site/'.$site->site_name.'/'.$site->id)}}}">{{{Str::limit($site->site_name, 15)}}}</a></li>
  	<li><a href="{{{$url}}}/settings">Settings</a></li>
  	<li><a href="" data-toggle="modal" data-target="#myModal">Create Event</a></li>
  		
  	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    @include('events.create')
	</div>
	</ul>
	@endif
	<div class="container pull-left" style="max-width:720px">

		@if($errors->all())
		<div class="bs-example">
		    <div class="alert alert-danger">
		        <a href="#" class="close" data-dismiss="alert">&times;</a>
		        <strong>Errors!</strong> Please check the errors.<br>
		        @if($errors->has('image'))
				<p class="text-danger">{{$errors->first('image')}}</p>
				@endif
		    </div>
		</div>
		@endif
		<h3>{{{$site->site_name}}}</h3>
		<div>
		@foreach($tags as $tag)
		| {{{ucfirst($tag->tag_name)}}}
		@endforeach
		</div>
		<div class="imagewrap">
		@if($site->site_pic !=NULL)
		<img src="../../{{$site->site_pic}}" class="img-responsive col-xs-12" style="padding:0"/>
		@else
		<img src="../../images/index.jpg" class="img-responsive col-xs-12" style="padding:0">
		@endif

		@if(!empty($admin))
		<form class="input-image-file" id="form" action="{{{URL::route('upload-photo-site-post', array('site_name' => $site->site_name, 'id' => $site->id))}}}" method="post"  enctype="multipart/form-data">

			<span class="glyphicon glyphicon-camera red glyphicon-file"><input type="file" name="image" id="file">
			</span>
			{{ Form::token() }}

			
		</form>

		<script>
			document.getElementById("file").onchange = function() {
    	document.getElementById("form").submit();
		};
		</script>
		@endif
		</div>
		<br>
		<div class="row">
			<div class="col-md-6 col-sm-6 col-xs-12">
				<p>
				<strong>About</strong><br>
				@if($site->about != NULL)
				{{{$site->about}}}
				@else
				
				@if(!empty($admin))
				<a href="{{$url}}/settings">Edit</a>
				@else
				<p>No info available</p>
				@endif
				@endif
				</p>
				@if($site->site_desc != NULL)
				<p>
				<strong>Description</strong><br>
				{{{$site->site_desc}}}
				</p>
				@endif
			</div>
			<div class="col-md-6 col-sm-6 col-xs-12">
				<div class="col-md-6 col-sm-6 col-xs-6" style="padding-left:0">
					<p>
					<strong>Address</strong><br>
					{{{$site->address}}}<br>
					{{{$site->postcode}}} {{{$site->city}}}, {{{$site->state}}}
					</p>
				</div>
				@if(($site->lat && $site->lng) != NULL)
				<div class="col-md-6 col-sm-6 col-xs-6">
					<div id="map-canvas" class="pull-right img-circle" style="border:2px solid #f3002d"></div>
				</div>
				@endif

				<div class="col-md-12 col-sm-12 col-xs-12" style="padding-left:0">
				@if($site->phone != NULL)
				Phone: <a href="tel:{{{$site->phone}}}">{{{$site->phone}}}</a><br>
				@endif
				@if($site->web != NULL)
				Website: <a href="{{{$site->web}}}" target="_blank">{{{$site->web}}}</a><br>
				@endif
				@if($site->fb != NULL)
				Facebook: <a href="{{{$site->fb}}}" target="_blank">{{{$site->fb}}}</a><br>
				@endif
				</div>
			
			</div>
		</div>
		<br>
		<div class="row">
			<div class="col-md-6 col-sm-6 col-xs-6">
				<div class="col-md-12 col-sm-12 col-xs-12" style="padding:0">

					<p><strong>Features</strong></p>
				</div>
				
				@if(!$checkedFeatures->isEmpty())
				@foreach(array_chunk($checkedFeatures->all(), 12) as $features)
					<div class="col-md-6 col-sm-6 col-xs-6" style="padding:0">
					@foreach ($features as $feature)
					<span class="glyphicon glyphicon-ok green"></span> {{{$feature->feature_name}}} <br>
					@endforeach
					<br>
					</div>
				@endforeach
				@else
				
					@if(!empty($admin))
					<a href="{{{$url}}}/settings#fe">Edit</a>
					@else
					<p>No feature available</p>
					@endif
				@endif
				</div>
			

			<div class="col-md-6 col-sm-6 col-xs-6">
				<p><strong>Operating Hours</strong></p>
				@if(!$ohours->isEmpty())
				<div id="Monday">
					<div style="float:left;">Monday</div>
					<div style="margin-left: 100px;">Closed</div>
				</div>
				<div id="Tuesday">
					<div style="float:left;">Tuesday</div>
					<div style="margin-left: 100px;">Closed</div>
				</div>
				<div id="Wednesday">
					<div style="float:left;">Wednesday</div>
					<div style="margin-left: 100px;">Closed</div>
				</div>
				<div id="Thursday">
					<div style="float:left;">Thursday</div>
					<div style="margin-left: 100px;">Closed</div>
				</div>
				<div id="Friday">
					<div style="float:left;">Friday</div>
					<div style="margin-left: 100px;">Closed</div>
				</div>
				<div id="Saturday">
					<div style="float:left;">Saturday</div>
					<div style="margin-left: 100px;">Closed</div>
				</div>
				<div id="Sunday">
					<div style="float:left;">Sunday</div>
					<div style="margin-left: 100px;">Closed</div>
				</div>
				@else
				@if(!empty($admin))
				<a href="{{$url}}/settings#oh">Edit</a>
				@else
				<p>No operating hours available</p>
				@endif
				@endif
			</div>

		</div>
		<br>

		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12 text-center" style="background-color:#f3002d; height:40px; line-height: 40px;">Events</div>
			<div class="col-md-4 col-sm-4 col-xs-4" style="border:1px solid #eeeeee; padding:0">
				<div class="event-list">
					@if(!$events->isEmpty())
					@foreach($events as $event)
					<div class="col-md-12 col-sm-12 col-xs-12 event-list-item {{{$event->id}}}">
						<div>{{{$event->event_name}}}</div>
    				</div>
    				@endforeach
    				@else
    				@if(!empty($admin))
					<a href="" data-toggle="modal" data-target="#myModal">Create Event</a>
  		
				  	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				    @include('events.create')
					</div>
					@else
					<p>No event available</p>
					@endif
					@endif
				</div>
				
			</div>
			<div class="col-md-8 col-sm-8 col-xs-8 event-list">
				@foreach($events as $event)
				
				<div class="col-md-12 col-sm-12 col-xs-12 event{{{$event->id}}} hide-event" style="padding:0">
					<a href="{{{url('event/'.$event->id)}}}" style="text-decoration:none">
					<div class="col-md-8 col-sm-8" style="padding:0">
						<img src="../../{{$event->event_pic}}" width="300px" class="img-responsive"/>
						<a href="{{{url('event/'.$event->id)}}}" style="text-decoration:none"><h4 style="padding-left:20px">{{{$event->event_name}}}</h4>
					</div>
					<div class="col-md-4 col-sm-4 col-xs-12">
						<p class="">
							<span class="glyphicon glyphicon-time green"></span> {{date("d M",$event->start / 1000)}} {{date("g:i:a",$event->start / 1000)}}
							<br>
						@if($event->start != $event->end)
							<span class="glyphicon glyphicon-time red"></span> {{date("d M",$event->end / 1000)}} {{date("g:i:a",$event->end / 1000)}}
						@endif
							<br>
							<span class="glyphicon glyphicon-map-marker grey"></span> {{{$event->location}}}
						</p>
					</div>
					<div class="col-md-12 col-sm-12 col-xs-12">
						<p><strong>Details: </strong>{{{$event->event_desc}}}</p>
					</div>
					</a>
					
    			</div>
    			
    			@endforeach
			</div>
		</div>



	</div>
</div>
<script>
@foreach($events as $event)
$('.{{$event->id}}').click(function() {
	$('.hide-event').hide();
    $('.event{{$event->id}}').show(); 
});
@endforeach
</script>

<script>
	var date = new Date();
	var today = date.getDay();
	var daysOfWeek = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
	var id = daysOfWeek[today];
	var ohours = {{$ohours}};

	for(var i in ohours){
		var parent = document.getElementById(ohours[i].days_of_week)
		var child = parent.childNodes;

		child[3].innerHTML = ohours[i].operating_hours;
	}

	document.getElementById(id).setAttribute("style", "font-weight:bold;");
</script>




{{HTML::script('js/bootstrap.js')}}
@stop
