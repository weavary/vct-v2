@extends('layouts.master')

@section('title')
<title>{{{$site->site_name}}}</title>
@stop

@section('meta')
<meta name="Author" content="VCT" />
<meta name="Keywords" content="{{{$site->about}}}" />
<meta name="Description" content="{{{$site->about}}}" />

<meta property="og:image" content="{{url($site->site_pic)}}" />
<meta property="og:url" content="{{{Request::url()}}}" />
<meta property="og:title" content="{{{$site->site_name}}}" />
<meta property="og:description" content="{{{$site->about}}}" />
@stop

@section('script')
{{HTML::style('css/bootstrap.css')}}
{{HTML::style('css/main.css')}}
{{HTML::style('css/jquery-ui.css')}}
{{HTML::style('css/jquery.tagit.css')}}

{{HTML::script('js/jquery-1.11.1.js')}}
{{HTML::script('js/jquery-ui.js')}}
{{HTML::script('js/tag-it.js')}}
@stop

@section('content')
<div class="container body-margin">
	@if(!empty($admin))
	<ul class="nav nav-tabs" role="tablist">
 		<li><a href="{{{url('site/'.$site->site_name.'/'.$site->id)}}}">{{{Str::limit($site->site_name, 15)}}}</a></li>
 		<li class="active"><a href="{{{$url}}}">Settings</a></li>
  		<li><a href="" data-toggle="modal" data-target="#myModal">Create Event</a></li>
 
  		<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		@include('events.create')
		</div>
	</ul>

	<div class="container pull-left" style="margin-top:10px;max-width:720px">
    	<form class="form-edit-site" role="form" action="{{{URL::route('edit-site-post', array('site_name' => $site->site_name, 'id' => $site->id))}}}" method="post">
        	<h2 class="form-edit-site-heading">Update Site</h2>
        	@if($errors->all())
		    <div class="bs-example">
		        <div class="alert alert-danger">
		        	<a href="#" class="close" data-dismiss="alert">&times;</a>
		            <strong>Errors!</strong> Please check the errors.<br>
		        </div>
		    </div>
			@endif
			

                <div class="form-group">
		    		<div class="input-group">
		    			<div class="input-group-addon">Category</div>
		    			<ul id="tag_list" title="Add 5 tags here!" style="margin:auto">
		                    @foreach($siteTags as $tag)
		                        <li>{{{$tag->tag_name}}}</li>
		                    @endforeach
		                </ul>
					</div>
				</div>


        		<div class="form-group margin-edit-site">
		    		<div class="input-group">
		    			<div class="input-group-addon">Site Name</div>
        				@if($errors->has('site_name'))
        				<input type="text" class="form-control" name="site_name" placeholder="Site Name" autofocus {{ (Input::old('site_name')) ? ' value="' . e(Input::old('site_name')) . '"' : '' }}>
		    			</div>
						</div>
						<p class="text-danger">{{$errors->first('site_name')}}</p>
						@else
						<input type="text" class="form-control" name="site_name" value="{{{$site->site_name}}}"{{ (Input::old('site_name')) ? ' value="' . e(Input::old('site_name')) . '"' : '' }}>
					</div>
				</div>
				@endif
				<div class="form-group margin-edit-site">
		    		<div class="input-group">
		    			<div class="input-group-addon">Address</div>
       					@if($errors->has('address'))
        				<input type="text" class="form-control" name="address" placeholder="Address" autofocus {{ (Input::old('address')) ? ' value="' . e(Input::old('address')) . '"' : '' }}>
		    			</div>
						</div>
						<p class="text-danger">{{$errors->first('address')}}</p>
						@else
						<input type="text" class="form-control" name="address" value="{{{$site->address}}}"{{ (Input::old('address')) ? ' value="' . e(Input::old('address')) . '"' : '' }}>
					</div>
				</div>
				@endif

				<div class="form-group margin-edit-site">
		    		<div class="input-group">
		    			<div class="input-group-addon">Postcode</div>
        				@if($errors->has('postcode'))
        				<input type="text" class="form-control" name="postcode" placeholder="Postcode" autofocus {{ (Input::old('postcode')) ? ' value="' . e(Input::old('postcode')) . '"' : '' }}>
		    			</div>
						</div>
						<p class="text-danger">{{$errors->first('postcode')}}</p>
						@else
						<input type="text" class="form-control" name="postcode" value="{{{$site->postcode}}}"{{ (Input::old('postcode')) ? ' value="' . e(Input::old('postcode')) . '"' : '' }}>
					</div>
				</div>
				@endif

				<div class="form-group margin-edit-site">
		    		<div class="input-group">
		    			<div class="input-group-addon">City</div>
							<input type="text" class="form-control" name="city" value="{{{$site->city}}}" disabled style="cursor:text; background-color:white">
						</div>
					</div>

					<div class="form-group margin-edit-site">
		    			<div class="input-group">
		    				<div class="input-group-addon">State</div>
							<input type="text" class="form-control" name="city" value="{{{$site->state}}}" disabled style="cursor:text; background-color:white">
						</div>
					</div>

					<div class="form-group margin-edit-site">
		    			<div class="input-group">
		    				<div class="input-group-addon">Phone</div>
							<input type="tel" class="form-control" name="phone" value="{{{$site->phone}}}"{{ (Input::old('phone')) ? ' value="' . e(Input::old('phone')) . '"' : '' }}>
						</div>
					</div>

        			<div class="form-group margin-edit-site">
		    			<div class="input-group">
		    				<div class="input-group-addon">Short Description</div>
							<input type="text" class="form-control" name="about" placeholder="To show in search result" value="{{{$site->about}}}"{{ (Input::old('about')) ? ' value="' . e(Input::old('about')) . '"' : '' }}>
						</div>
					</div>
		
					<div class="form-group margin-edit-site">
		    			<div class="input-group">
		    				<div class="input-group-addon">Long Description</div>
		    				<textarea class="form-control" rows="4" placeholder="Details about the organization" name="site_desc">{{{$site->site_desc}}}</textarea>
						</div>
					</div>

					<div class="form-group margin-edit-site">
		    			<div class="input-group">
		    				<div class="input-group-addon">Website</div>
							@if($errors->has('web'))
        					<input type="text" class="form-control" name="web" placeholder="eg.: https://www.vct.my" {{ (Input::old('web')) ? ' value="' . e(Input::old('web')) . '"' : '' }}>
		    				</div>
							</div>
							<p class="text-danger">{{$errors->first('web')}}</p>
							@else
							<input type="text" class="form-control" name="web" placeholder="eg.: https://www.vct.my" value="{{{$site->web}}}" {{ (Input::old('web')) ? ' value="' . e(Input::old('web')) . '"' : '' }}>
						</div>
					</div>
					@endif

					<div class="form-group margin-edit-site">
		    			<div class="input-group">
		    				<div class="input-group-addon">Facebook</div>
							@if($errors->has('facebook'))
        					<input type="text" class="form-control" name="facebook" {{ (Input::old('facebook')) ? ' value="' . e(Input::old('facebook')) . '"' : '' }}>
		    				</div>
							</div>
							<p class="text-danger">{{$errors->first('facebook')}}</p>
							@else
							<input type="text" class="form-control" name="facebook"  placeholder="eg.: https://www.fb.com/vctonline" value="{{$site->fb}}" {{ (Input::old('facebook')) ? ' value="' . e(Input::old('facebook')) . '"' : '' }} />
						</div>
					</div>
					@endif
					<div class="form-group margin-edit-site">
		    			<div class="input-group">
		    				<div class="input-group-addon">GPS Latitude Coordinates</div>
							<input type="text" class="form-control" name="latitude"  placeholder="eg. : 4.32766" value="{{$site->lat}}" {{ (Input::old('latitude')) ? ' value="' . e(Input::old('latitude')) . '"' : '' }} />
						</div>
					</div>
					<div class="form-group margin-edit-site">
		    			<div class="input-group">
		    				<div class="input-group-addon">GPS Longitude Coordinates</div>
							<input type="text" class="form-control" name="longitude"  placeholder="eg. : 101.146006" value="{{$site->lng}}" {{ (Input::old('longitude')) ? ' value="' . e(Input::old('longitude')) . '"' : '' }} />
						</div>
					</div>
					<br>
					<!-- /* edited area -->
           			

					




					<div class="panel panel-default" id="fe">
						<div class="panel-heading">
						<h3 class="panel-title">Features</h3>
						</div>
						<div class="panel-body">
							<div class="row">
								<div class="col-md-3 col-sm-3 col-xs-6">
									<div class="form-group">
				                		<label for="">Facilities</label>

				                		<ul id="general_features" style="list-style-type:none; padding:0; margin:0;">
											@foreach($facilitiesFeatures as $feature)
												<li><input type="checkbox" id="{{{$feature->id}}}" name="features[]" value="{{{$feature->id}}}"> {{{$feature->feature_name}}}</li>
											@endforeach
										</ul>
									</div>
								</div>
								<div class="col-md-3 col-sm-3 col-xs-6">
									<div class="form-group">
				                		<label for="">Environment</label>

				                		<ul id="fnb_features" style="list-style-type:none; padding:0; margin:0;">
											@foreach($environmentFeatures as $feature)

												<li><input type="checkbox" id="{{{$feature->id}}}" name="features[]" value="{{{$feature->id}}}"> {{{$feature->feature_name}}}</li>
											@endforeach
										</ul>
									</div>
								</div>
								<div class="clearfix visible-xs"></div>
								<div class="col-md-3 col-sm-3 col-xs-6">
									<div class="form-group">
				                		<label for="">Services</label>

				                		<ul id="stay_features" style="list-style-type:none; padding:0; margin:0;">
											@foreach($servicesFeatures as $feature)
												<li><input type="checkbox" id="{{{$feature->id}}}" name="features[]" value="{{{$feature->id}}}"> {{{$feature->feature_name}}}</li>
											@endforeach
										</ul>
									</div>
								</div>
								<div class="col-md-3 col-sm-3 col-xs-6">
									<div class="form-group">
				                		<label for="">Payment Method</label>

				                		<ul id="stay_features" style="list-style-type:none; padding:0; margin:0;">
											@foreach($paymentFeatures as $feature)
												<li><input type="checkbox" id="{{{$feature->id}}}" name="features[]" value="{{{$feature->id}}}"> {{{$feature->feature_name}}}</li>
											@endforeach
										</ul>
									</div>
								</div>
							</div>

                

						</div>
					</div>

					<div class="panel panel-default" id="oh">
						<div class="panel-heading">
						<h3 class="panel-title">Operating Hours</h3>
						</div>
						<div class="panel-body">
							<div id="oh_list"></div>
						<select id="days_of_week">
                			<option value="1">Monday</option>
                			<option value="2">Tuesday</option>
                			<option value="3">Wednesday</option>
                			<option value="4">Thursday</option>
                			<option value="5">Friday</option>
                			<option value="6">Saturday</option>
                			<option value="7">Sunday</option>
                		</select>

                		<select id="from_hour">
                			<option>12</option>
                			<option>1</option>
                			<option>2</option>
                			<option>3</option>
                			<option>4</option>
                			<option>5</option>
                			<option>6</option>
                			<option>7</option>
                			<option>8</option>
                			<option>9</option>
                			<option>10</option>
                			<option>11</option>
                		</select>
                		<label>:</label>
                		<select id="from_minute">
                			<option>00</option>
                			<option>15</option>
                			<option>30</option>
                			<option>45</option>
                		</select>

                		<select id="from_period">
                			<option>am</option>
                			<option>pm</option>
                		</select>

                		<label>&nbsp;to&nbsp;</label>
                		<select id="to_hour">
                			<option>12</option>
                			<option>1</option>
                			<option>2</option>
                			<option>3</option>
                			<option>4</option>
                			<option>5</option>
                			<option>6</option>
                			<option>7</option>
                			<option>8</option>
                			<option>9</option>
                			<option>10</option>
                			<option>11</option>
                		</select>
                		<label>:</label>
                		<select id="to_minute">
                			<option>00</option>
                			<option>15</option>
                			<option>30</option>
                			<option>45</option>
                		</select>

                		<select id="to_period">
                			<option>am</option>
                			<option>pm</option>
                		</select>

                		<input type="button" value="Add" onClick="addToList()"/>
						</div>
					</div>

					
                	<!-- edited area */ -->
        			<button class="btn btn-primary btn-block" type="submit">Update</button>
        			{{ Form::token() }}


    			</form>

    			<script>
	var chkf = {{$checkedFeatures}};

	for(var i = 0; i < chkf.length; i++)
		document.getElementById(chkf[i].id).checked = true;
</script>

<script>
	var ohours = {{$ohours}};
	var list = document.getElementById("oh_list");

	for(var i = 0; i < ohours.length; i++){
		var item = document.createElement("div");
	    var operatingHours = document.createElement("input");
	    var btnRemove = document.createElement("input");
	    var listCount = list.childNodes.length;
	    var daysOfWeek = ohours[i].days_of_week;
	    
	    if(daysOfWeek == "Monday") var id = "oh1";
    	if(daysOfWeek == "Tuesday") var id = "oh2";
		if(daysOfWeek == "Wednesday") var id = "oh3";
		if(daysOfWeek == "Thursday") var id = "oh4";
		if(daysOfWeek == "Friday") var id = "oh5";
		if(daysOfWeek == "Saturday") var id = "oh6";
		if(daysOfWeek == "Sunday") var id = "oh7";

		item.setAttribute("id", id);

	    operatingHours.setAttribute("name", "operatingHours[]");
	    operatingHours.setAttribute("style", "width:333px");
	    operatingHours.setAttribute("readonly", true);
	    operatingHours.setAttribute("value", ohours[i].days_of_week + "," + ohours[i].operating_hours);

	    btnRemove.setAttribute("type", "button");
	    btnRemove.setAttribute("id", id);
	    btnRemove.setAttribute("value", "Remove");
	    btnRemove.setAttribute("onClick", "removeFromList(this.id)");
	
	    item.appendChild(operatingHours);
	    item.appendChild(btnRemove);
	    list.appendChild(item);
	}
</script>

<script>
	function addToList() {
		var list = document.getElementById("oh_list");
		var newItem = document.createElement("div");
	    var operatingHours = document.createElement("input");
	    var btnRemove = document.createElement("input");
	    var listCount = list.childNodes.length;
		var daysOfWeek = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"];
		var dayIndex = document.getElementById("days_of_week").value - 1;
		var id = "oh" + document.getElementById("days_of_week").value;
		var oldItem = document.getElementById(id);
		var openTime = document.getElementById("from_hour").value + ":" + 
			document.getElementById("from_minute").value + " " + 
	    	document.getElementById("from_period").value
	    var closeTime = document.getElementById("to_hour").value + ":" + 
	    	document.getElementById("to_minute").value + " " + 
	    	document.getElementById("to_period").value

	    newItem.setAttribute("id", id);

	    operatingHours.setAttribute("name", "operatingHours[]");
	    operatingHours.setAttribute("style", "width:333px");
	    operatingHours.setAttribute("readonly", true);

	    if(openTime == closeTime)
	    	operatingHours.setAttribute("value", daysOfWeek[dayIndex] + ", 24 Hours");
	    else
		    operatingHours.setAttribute("value", daysOfWeek[dayIndex] + ", " + openTime + " - " + closeTime);

	    btnRemove.setAttribute("id", id);
	    btnRemove.setAttribute("type", "button");
	    btnRemove.setAttribute("value", "Remove");
	    btnRemove.setAttribute("onClick", "removeFromList(this.id)");
	
	    newItem.appendChild(operatingHours);
	    newItem.appendChild(btnRemove);

	    if(oldItem == null) list.appendChild(newItem);
	    else list.replaceChild(newItem, oldItem);

	    // http://stackoverflow.com/questions/282670/easiest-way-to-sort-dom-nodes
		var list = document.getElementById('oh_list');
		var item = list.childNodes;
		var itemArr = [];

		for(var i in item)
			if(item[i].nodeType == 1)
				itemArr.push(item[i]);

		itemArr.sort(function(a, b){
			return a.getAttribute("id").localeCompare(b.getAttribute("id"));
		});

		for(i = 0; i < itemArr.length; ++i)
			list.appendChild(itemArr[i]);
	}
</script>

<script>
	function removeFromList(EleId) {
	    document.getElementById(EleId).remove();
	}
</script>
		
    		
		        <script type="text/javascript">
	            $(document).ready(function(){
	                $('#tag_list').tagit({
	                    allowSpaces: true,
	                    autocomplete: {
	                        source: function(request, response){
	                            $.getJSON('/api/tags', {term: request.term}, response);
	                        }
	                    },
	                    fieldName: 'tags[]',
	                    tagLimit: 5
	                });
	            });

	            var chkf = {{$checkedFeatures}};

		        	for(var i = 0; i < chkf.length; i++)
		        		document.getElementById(chkf[i].id).checked = true;
        		</script>
  		
	</div>

</div>    

@else
@include('layouts.error-page')
@endif



</div>
{{HTML::script('js/bootstrap.js')}}
@stop
