@extends('layouts.master')

@section('title')
<title>Create a Site</title>
@stop

@section('meta')
<meta name="Author" content="VCT" />
<meta name="Keywords" content="Create a Site" />
<meta name="Description" content="Create a site" />

<meta property="og:image" content="{{url('images/index.jpg')}}" />
<meta property="og:url" content="{{{Request::url()}}}" />
<meta property="og:title" content="Create a Site" />
<meta property="og:description" content="Create a site" />
@stop

@section('script')
{{HTML::style('css/bootstrap.css')}}
{{HTML::style('css/main.css')}}

{{HTML::script('js/jquery-1.11.1.js')}}
@stop

@section('content')
<div class="container body-margin">
    <form class="form-create-site" role="form" action="{{URL::route('create-site-post')}}" method="post">
        <h2 class="form-create-site-heading text-center">Create a Site</h2>
        @if($errors->all())
            <div class="bs-example">
                <div class="alert alert-danger">
                    <a href="#" class="close" data-dismiss="alert">&times;</a>
                    <strong>Errors!</strong> Please check the errors.<br>
                </div>
            </div>
        @endif
        <input type="text" class="form-control" name="site_name" placeholder="Site Name" {{(Input::old('site_name')) ? ' value="' . e(Input::old('site_name')) . '"' : '' }} autofocus>
        @if($errors->has('site_name'))
		<p class="text-danger">{{$errors->first('site_name')}}</p>
		@endif
        <input type="text" class="form-control" name="address" placeholder="Address" {{(Input::old('address')) ? ' value="' . e(Input::old('address')) . '"' : '' }}>
        @if($errors->has('address'))
		<p class="text-danger">{{{$errors->first('address')}}}</p>
		@endif
        <input type="text" class="form-control" name="postcode" placeholder="Postcode" {{(Input::old('postcode')) ? ' value="' . e(Input::old('postcode')) . '"' : '' }}>
        @if($errors->has('postcode'))
		<p class="text-danger">{{$errors->first('postcode')}}</p>
		@endif
        <input type="text" class="form-control" name="city" value="Kampar, Perak" disabled style="cursor:text; background-color:white">
        <button class="btn btn-primary btn-block" type="submit">Get Started</button>
        {{Form::token() }}
    </form>
</div>
{{HTML::script('js/bootstrap.js')}}
@stop