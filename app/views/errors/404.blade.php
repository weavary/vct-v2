@extends('layouts.master')

@section('title')
<title>Page Not Found</title>
@stop

@section('meta')
<meta name="Author" content="VCT" />
<meta name="Keywords" content="VCT" />
<meta name="Description" content="404 Page Not Found" />

<meta property="og:image" content="{{url('images/index.jpg')}}" />
<meta property="og:url" content="{{{Request::url()}}}" />
<meta property="og:title" content="404 Page Not Found" />
<meta property="og:description" content="404 Page Not Found" />
@stop


@section('script')
{{HTML::style('css/bootstrap.css')}}
{{HTML::style('css/main.css')}}

{{HTML::script('js/jquery-1.11.1.js')}}



@stop

@section('content')
<div class="container body-margin">
@include('layouts.error-page')
</div>
{{HTML::script('js/bootstrap.js')}}
@stop
