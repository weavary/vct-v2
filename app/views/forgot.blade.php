@extends('layouts.master')

@section('title')
<title>Reset Your Password</title>
@stop

@section('meta')
<meta name="Author" content="VCT" />
<meta name="Keywords" content="Forgotten, Password, Reset" />
<meta name="Description" content="Reset password" />

<meta property="og:image" content="{{url('images/index.jpg')}}" />
<meta property="og:url" content="{{{Request::url()}}}" />
<meta property="og:title" content="Reset Your Password" />
<meta property="og:description" content="reset password" />
@stop

@section('script')
{{HTML::style('css/bootstrap.css')}}
{{HTML::style('css/main.css')}}

{{HTML::script('js/jquery-1.11.1.js')}}
@stop

@section('content')
<div class="container body-margin">
    
    <form class="form-join" role="form" action="{{URL::route('forgot-post')}}" method="post">
        <h2 class="form-join-heading text-center">Forgotten Password</h2>
        @if($errors->all())
        <div class="bs-example">
            <div class="alert alert-danger">
                <a href="#" class="close" data-dismiss="alert">&times;</a>
                <strong>Errors!</strong> Please check the errors.<br>
            </div>
        </div>
        @endif
        <input type="email" class="form-control" name="email" placeholder="Email address" {{ (Input::old('email')) ? ' value="' . e(Input::old('email')) . '"' : '' }} autofocus>
        @if($errors->has('email'))
		<p class="text-danger">{{$errors->first('email')}}</p>
		@endif
        <button class="btn btn-lg btn-primary btn-block" type="submit">Recover</button>
        {{ Form::token() }}
    </form>
</div>
{{HTML::script('js/bootstrap.js')}}
@stop
