<?php

class Pizza extends Eloquent{
	protected $fillable = array(
							'name',
							'email',
							'address',
							'postcode',
							'city',
							'phone',
							'tshirt',
							'vege',
							'receipt',
							'pledge'
	);

	protected $table = 'pizzas';

}