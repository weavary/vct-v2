<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class People extends Eloquent implements UserInterface, RemindableInterface {

	protected $fillable = array(
							'email',
							'first_name',
							'last_name',
							'dob',
							'phone',
							'password',
							'password_temp',
							'code',
							'active'	
							);

	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'peoples';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'remember_token');

	public function sites()
    {
        return $this->belongsToMany('Site', 'people_site', 'people_id', 'site_id');
    }

    public function photos(){
    	return $this->morphMany('Photo', 'photoable');
    }

    public function tags(){
    	return $this->belongsToMany('tag');
    }

}
