<?php

class Tag extends Eloquent{
	protected $fillable = array(
							'tag_name',
	);

	protected $table = 'tags';

	public function sites()
    {
        return $this->belongsToMany('Site');
    }

}