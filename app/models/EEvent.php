<?php

class EEvent extends Eloquent{
	protected $fillable = array(
							'event_name',
							'event_desc',
							'location',
							'event_pic',
							'start',
							'end',
							'site_id',
							'people_id'
	);

	protected $table = 'events';

	public function site(){
		return $this->belongsTo('Site');
	}

	public function photos(){
    	return $this->morphMany('Photo', 'photoable');
    }

}