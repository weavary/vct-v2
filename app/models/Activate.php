<?php

class Activate extends Eloquent{
	protected $fillable = array(
							'email',
							'password',
							'code',
							'active',
	);

	protected $table = 'activates';
}