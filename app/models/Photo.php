<?php

class Photo extends Eloquent{
	protected $fillable = array(
							'caption',

	);

	protected $table = 'photos';

	public function imageable()
    {
        return $this->morphTo();
    }
}