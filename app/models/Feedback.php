<?php

class Feedback extends Eloquent{
	protected $fillable = array(
							'name',
							'email',
							'emo',
							'msg',
	);

	protected $table = 'feedbacks';
}