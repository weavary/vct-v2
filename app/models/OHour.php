<?php

class OHour extends Eloquent{
	protected $fillable = array(
							'site_id',
							'days_of_week',
							'operating_hours'
	);

	protected $table = 'operating_hours';

	public function site(){
		return $this->belongsTo('Site');
	}
}