<?php

class Feature extends Eloquent{
	protected $fillable = array(
							'type',
							'feature_name',

	);

	protected $table = 'features';

	public function sites()
    {
        return $this->belongsToMany('Site', 'feature_site', 'feature_id', 'site_id');
    }
}