<?php

class Site extends Eloquent{
	protected $fillable = array(
							'site_name',
							'address',
							'postcode',
							'city',
							'state',
							'country',
							'lat',
							'lng',
							'phone',
							'web',
							'fb',
							'site_pic',
							'active'
	);

	protected $table = 'sites';

	public function peoples()
    {
        return $this->belongsToMany('People', 'people_site', 'site_id', 'people_id');
    }

    public function events(){
    	return $this->hasMany('EEvent');
    }

    public function features(){
    	return $this->belongsToMany('Feature', 'feature_site', 'site_id', 'feature_id');
    }

    public function photos(){
    	return $this->morphMany('Photo', 'photoable');
    }

    public function tags(){
    	return $this->belongsToMany('Tag');
    }
    public function bhours(){
		return $this->hasMany('OHour');
	}
}