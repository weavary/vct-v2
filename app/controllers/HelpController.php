<?php

class HelpController extends BaseController{
	
	public function help(){
		return View::make('helps.index');
	}

	public function howtos(){
		return View::make('helps.index');
	}

	public function faq(){
		return View::make('helps.faq');
	}

	public function getFeedback(){
		return View::make('helps.feedback');
	}

	public function postFeedback(){
		$validator = Validator::make(Input::all(),
			array(
				'name' => 'required|min:3',
				'email' => 'required|email',
				'emotional_state' => 'required',
				'message' => 'required|min:5'
			)
		);

		if($validator->passes()){
			$name = Input::get('name');
			$email = Input::get('email');
			$emotional_state = Input::get('emotional_state');
			$message = Input::get('message');

			$feedback = Feedback::create(array(
				'name' => $name,
				'email' => $email,
				'emo' => $emotional_state,
				'msg' => $message,
			));


				Mail::send('emails.auth.feedback', 
					array('msg' => $message, 'name' => $name, 'emo' => $emotional_state), 
					function($message)use ($feedback){
				   	 $message->to($feedback->email, $feedback->name)->bcc('hello@vct.my')->subject('Thank You for Your Feedback');
				});
				return Redirect::route('helps-feedback')
					->with('feedback-success', "Thank you for your feedback.");
			}else{
				return Redirect::route('helps-feedback')
					->withErrors($validator)
					->withInput();
		}
	}
}