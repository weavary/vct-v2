<?php

class ImageController extends BaseController {

	public function showHome(){
		return View::make('phpinfo');
	}

	public function showImageForm(){
		return View::make('imageForm');
	}

	public function postImageForm(){
		Validator::extend('dimension_min', function($attribute, $value, $parameters)
		{
	        $file = Input::file($attribute);
	        $dimension = getimagesize($file);
	        $width = $dimension[0];
	        $height = $dimension[1];
	        if((isset($parameters[0]) && $parameters[0] != 0) && $width < $parameters[0]) return false;
	        if((isset($parameters[1]) && $parameters[1] != 0) && $height < $parameters[1] ) return false;
	        return true;
		});

		Validator::extend('dimension_max', function($attribute, $value, $parameters)
		{
	        $file = Input::file($attribute);
	        $dimension = getimagesize($file);
	        $width = $dimension[0];
	        $height = $dimension[1];
	        if((isset($parameters[0]) && $parameters[0] != 0) && $width > $parameters[0]) return false;
	        if((isset($parameters[1]) && $parameters[1] != 0) && $height > $parameters[1] ) return false;
	        return true;
		});

		$rules = array(
			'image' => 'required'
		);

	

		$validation = Validator::make(Input::all(), $rules);

		if($validation->passes()){
			$file = Input::file('image');
			$cln_fname = $file->getClientOriginalName();
			$ext = explode("/", $file->getMimeType());
			$svr_fname = time() . '_' . uniqid() . '.jpeg';
			$svr_fpath = "images/original/" . $svr_fname;

			if($ext[1] == "jpeg"){
				$img = imagecreatefromjpeg($file);
			}
			else if($ext[1] == "png"){
				$img = imagecreatefrompng($file);
			}
			else{
				$img = imagecreatefromgif($file);
			}

			$qlt = 90;
			$new_w = 800;
			$org_w = imagesx($img);
			$org_h = imagesy($img);
			$new_h = (($org_h * $new_w) / $org_w);
			$dest = imagecreatetruecolor($new_w, $new_h);

			imagecopyresized(
				$dest, $img, 
				0, 0, 0, 0,
			    $new_w, $new_h,
			    $org_w, $org_h
			);

			if(imagejpeg($dest, $svr_fpath, $qlt)){
				imagedestroy($dest);
				return Redirect::to('jcrop')->with('image', $svr_fname);
			}
			else{
				return "Error uploading file.";
			}
		}
		else{
			return Redirect::to('imageForm')->withErrors($validation);
		}
	}

	public function showJcrop(){
		return View::make('jcrop')->with('image', 'images/original/' . Session::get('image'));
	}

	public function postJcrop(){
		$qlt = 90;
		$src = Input::get('image');
		$org_w = Input::get('w');
		$org_h = Input::get('h');
		$new_w = 800;
		$new_h = (($org_h * $new_w) / $org_w);
		$img = imagecreatefromjpeg($src);
		$crop = imagecreatetruecolor($org_w, $org_h);
		$dest = imagecreatetruecolor($new_w, $new_h);
		$ext = pathinfo($src, PATHINFO_EXTENSION);

		imagecopyresampled(
			$crop, $img, 0, 0, 
			Input::get('x'), 
			Input::get('y'), 
			Input::get('w'), 
			Input::get('h'), 
			$org_w, 
			$org_h
		);

		imagecopyresized(
			$dest, $crop, 
			0, 0, 0, 0,
		    $new_w, $new_h,
		    $org_w, $org_h
		);

		$src = "images/crop/" . basename($src, "." . $ext) . '_crop.' . $ext;
		imagejpeg($dest, $src, $qlt);
		imagedestroy($dest);

		return "<img src='" . $src . "'>";
	}
}
