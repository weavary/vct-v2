<?php

class SiteController extends BaseController {

	public function site($site_name, $id){
		$site = Site::where('id', '=', $id)
			->where('site_name', '=', $site_name)
			->first();
		if($site){
		$url = Request::url();

		$admin = NULL;
		if(Auth::check()){
			$people = People::find(Auth::user()->id);;
			$admin= $people->sites->contains($site->id);
		}

		$checkedFeatures = $site->features;
		$ohours = OHour::where('site_id', '=', $site->id)->get();

		$time_now = Carbon::now(new DateTimeZone('Asia/Kuala_Lumpur'));
		$time_now = strtotime($time_now) * 1000;


		$events = EEvent::where('site_id', '=', $id)
			->where('end', '>', $time_now)
			->orderBy('start', 'asc')->get();


		$tags = Site::find($id)->tags;

		
			return View::make('sites.index')
				->with('site', $site)
				->with('url', $url)
				->with('admin', $admin)
				->with('checkedFeatures', $checkedFeatures)
				->with('tags', $tags)
				->with('ohours', $ohours)
				->with('events', $events);
		}else{
				return App::abort(404);
	
		}
	}

	public function getCreateSite(){
		return View::make('sites.create');
	}



	public function postCreateSite(){
		Validator::extend('alpha_num_spaces_dot_dash_under', function($attribute, $value)
	{
	    return preg_match('/^[a-z0-9 .\-\_]+$/i', $value);
	});

		$validator = Validator::make(Input::all(),
			array(
				'site_name' => 'required|min:3|max:75|alpha_num_spaces_dot_dash_under',
				'address' => 'required|min:3',
				'postcode' => 'required|size:5|digits:5',
		));

		if($validator->passes()){
			$site_name = Input::get('site_name');
			$address = Input::get('address');
			$postcode = Input::get('postcode');
			$city = 'Kampar';
			$state = 'Perak';
			$country = 'Malaysia';
			$site_pic = 'images/index.jpg';
			$active = 1;

			$site = Site::create(array(
				'site_name' => $site_name,
				'address' => $address,
				'postcode' => $postcode,
				'city' => $city,
				'state' => $state,
				'country' => $country,
				'site_pic' => $site_pic,
				'active' => $active,
			));

			$people = People::find(Auth::user()->id);

			$people->sites()->save($site);

			if($site){
				return Redirect::route('profile-site', array('site_name' => $site_name, 'id' => $site->id));
			}

		}else{
			return Redirect::route('create-site')
				->withErrors($validator)
				->withInput();
		}
	}

	public function getSettingsSite($site_name, $id){
		$site = Site::where('id', '=', $id)
			->where('site_name', '=', $site_name)
			->first();

		// /* edited area
		$ohours = OHour::where('site_id', '=', $site->id)->get();
		$facilitiesFeatures = Feature::where('type', '=', 'facilities')->orderBy('feature_name')->get();
		$environmentFeatures = Feature::where('type', '=', 'environment')->orderBy('feature_name')->get();
		$servicesFeatures = Feature::where('type', '=', 'services')->orderBy('feature_name')->get();
		$paymentFeatures = Feature::where('type', '=', 'payment')->orderBy('feature_name')->get();

		$checkedFeatures = $site->features;
		// edited area */
		
		$url = Request::url();

		$people = People::find(Auth::user()->id);;
		$admin = $people->sites->contains($site->id);

		//$site_id = 1000000001;
		$siteTags = Site::find($site->id)->tags;
		//$sites = Site::find($site_id);
		//return View::make('tagit')->with('siteTags', $siteTags)->with('sites', $sites);


		return View::make('sites.edit')
			->with('site', $site)
			->with('facilitiesFeatures', $facilitiesFeatures)			// edited area
			->with('environmentFeatures', $environmentFeatures)			// edited area
			->with('servicesFeatures', $servicesFeatures)			// edited area
			->with('paymentFeatures', $paymentFeatures)			// edited area
			->with('checkedFeatures', $checkedFeatures)	// edited area
			->with('ohours', $ohours)
			->with('url', $url)
			->with('siteTags', $siteTags)
			->with('admin', $admin);
	}

	public function postSettingsSite($site_name, $id){


		Validator::extend('alpha_num_spaces_dot_dash_under', function($attribute, $value)
		{
		    return preg_match('/^[a-z0-9 .\-\_]+$/i', $value);
		});
		$validator = Validator::make(Input::all(),
			array(
				'site_name' => 'required|min:3|max:75|alpha_num_spaces_dot_dash_under',
				'address' => 'required',
				'postcode' => 'required|size:5|digits:5',
				'web' => 'url',
				'facebook' => 'url',
 			)
		);
		$site = Site::where('id', '=', $id)->first();
		$people = People::find(Auth::user()->id);;
		$admin = $people->sites->contains($site->id);



		if($validator->passes()){
			$site_name = Input::get('site_name');
			$address = Input::get('address');
			$postcode = Input::get('postcode');
			$phone = Input::get('phone');
			$about = Input::get('about');
			$site_desc = Input::get('site_desc');
			$web = Input::get('web');
			$fb = Input::get('facebook');
			$latitude = Input::get('latitude');
			$longitude = Input::get('longitude');



			//$check = Input::get('credit');

			// /* edited area

			$features = Input::get('features', array());
			$operatingHours = Input::get('operatingHours', array());

			$site->features()->detach();
			foreach($features as $id){
				$feature = Feature::find($id);
				$site->features()->save($feature);
			}
	        // edited area */

	        OHour::where('site_id', '=', $site->id)->delete();

	        foreach($operatingHours as $operatingHour){
				$splitTime = explode(',', $operatingHour);
				$bhour = OHour::Create(
					array(
						'site_id' => $site->id, 
						'days_of_week' => $splitTime[0], 
						'operating_hours' => $splitTime[1]
					)
				);
			}

			$site->site_name = $site_name;
			$site->address = $address;
			$site->postcode = $postcode;
			$site->phone = $phone;
			$site->about = $about;
			$site->site_desc = $site_desc;
			$site->web = $web;
			$site->fb = $fb;
			$site->lat = $latitude;
			$site->lng = $longitude;
			$site->save();

	        $tags = Input::get('tags', array()); 
	        $site->tags()->detach();

	        foreach($tags as $name) {
	            $tag = Tag::firstOrCreate(array('tag_name' => $name));
	            $site->tags()->save($tag);
	        }

	  

			if($site){
				return Redirect::route('edit-site', array('site_name' => $site_name, 'id' => $site->id))
					->with('global-success', 'Updated site info.');
			}
		}else{
			return Redirect::route('edit-site', array('site_name' => $site_name, 'id' => $site->id))
				->withErrors($validator)
				->withInput();
		}

	}


	public function postUploadPhotoSite($site_name, $id){

		$validation = Validator::make(Input::all(), 
			array(
				'image' => 'required|mimes:jpeg,png,bmp,gif|max:15360'
		));

		$site = Site::where('id', '=', $id)->first();


		if($validation->passes()){
			$file = Input::file('image');
			$org_filename = $file->getClientOriginalName();
			$ext = explode("/", $file->getMimeType());
			$ext = $ext[1];

			/* filename */
			$unique_site_id = crc32($site->id);
			$unique_filename = crc32($org_filename);
			$unique_user_id = crc32(Auth::user()->id);

			$final_filename = time() .'_'.$unique_site_id.'_'.$unique_filename.'_'.$unique_user_id.'.'.$ext;
			$final_filepath = "photos/temp/" . $final_filename;
			$width = Image::make($file)->width();

			if($width >= 900){
				$width = 900;
			}else{
				$width = $width;
			}


			if($file){
				Image::make($file->getRealPath())->resize($width, null, function ($constraint) {$constraint->aspectRatio();})->save($final_filepath);
				return Redirect::route('crop-photo-site', array('site_name' => $site_name, 'id' => $id))
					->with('image', $final_filename)
					->with('site', $site);
			}
			else{
				return Redirect::route('profile-site', array('site_name' => $site_name, 'id' => $id))
					->with('global-error', 'Failed to upload, please try again.');
			}
		}
		else{
			return Redirect::route('profile-site', array('site_name' => $site_name, 'id' => $id))
				->withErrors($validation);
		}
	}

	public function getCropPhotoSite($site_name, $id){
		$site = Site::where('id', '=', $id)->first();

		$admin = NULL;
		if(Auth::check()){
			$people = People::find(Auth::user()->id);;
			$admin= $people->sites->contains($site->id);
		}

		$events = EEvent::where('site_id', '=', $id)->get();

		return View::make('sites.crop', array('site_name' => $site_name, 'id' => $id))
			->with('site', $site)
			->with('events', $events)
			->with('admin', $admin);
	}

	public function postCropPhotoSite($site_name, $id){

		$src = Input::get('image');
		$ext = pathinfo($src, PATHINFO_EXTENSION);

		$src_path = "photos/" . basename($src, "." . $ext) . '_c.' . $ext;

		$int_img = Image::make('photos/temp/'.$src);
		$int_img->crop(intval(Input::get('w')), intval(Input::get('h')), intval(Input::get('x')), intval(Input::get('y')));
		$int_img->fit(720, 405);
		$int_img->save($src_path);

		$site = Site::where('id', '=', $id)->first();
		$site->site_pic = $src_path;
		$site->save();

		$photo = new Photo;
		$photo->path = $src_path;
		$photo->save();
		$site->photos()->save($photo);


		if($site && $photo){
			return Redirect::route('profile-site', array('site_name' => $site_name, 'id' => $id))
				->with('global-success', 'Successfully updated photo.');
		}else{
			return Redirect::route('profile-site', array('site_name' => $site_name, 'id' => $id))
					->with('global-error', 'Failed to upload, please try again.');
		}

		return Redirect::route('profile-site', array('site_name' => $site_name, 'id' => $id))
					->with('global-error', 'Failed to upload, please try again.');
		
	}

}
