<?php

class PeopleController extends BaseController{
	
	public function people($id){


		$people = People::where('id', '=', $id)
			->first();

		if($people){
			return View::make('peoples.index')
				->with('people', $people);
		}else{
			return App::abort(404);
		}
	}

	public function postUploadPhotoPeople($id){

		$validation = Validator::make(Input::all(), 
			array(
				'image' => 'required|mimes:jpeg|max:15360'
		));

		$people = People::where('id', '=', $id)->first();


		if($validation->passes()){
			$file = Input::file('image');
			$org_filename = $file->getClientOriginalName();
			$ext = explode("/", $file->getMimeType());
			$ext = $ext[1];

			/* filename */
			$unique_filename = crc32($org_filename);
			$unique_user_id = crc32(Auth::user()->id);

			$final_filename = time() .'_'.$unique_filename.'_'.$unique_user_id.'.'.$ext;
			$final_filepath = "photos/temp/" . $final_filename;
			$width = Image::make($file)->width();

			if($width >= 900){
				$width = 900;
			}else{
				$width = $width;
			}


			if($file){
				Image::make($file->getRealPath())->resize($width, null, function ($constraint) {$constraint->aspectRatio();})->save($final_filepath);
				return Redirect::route('crop-photo-people', array('id' => $id))
					->with('image', $final_filename)
					->with('people', $people);
			}
			else{
				return Redirect::route('profile-people', array('id' => $id))
					->with('global-error', 'Failed to upload, please try again.');
			}
		}
		else{
			return Redirect::route('profile-people', array('id' => $id))
				->withErrors($validation);
		}
	}

	public function getCropPhotoPeople($id){
		$people = People::where('id', '=', $id)->first();

		$admin = NULL;
		if(Auth::check()){
		}

		return View::make('peoples.crop', array('id' => $id))
			->with('people', $people);
	}

	public function postCropPhotoPeople($id){

		$src = Input::get('image');
		$ext = pathinfo($src, PATHINFO_EXTENSION);

		$src_path = "photos/" . basename($src, "." . $ext) . '_c.' . $ext;

		$int_img = Image::make('photos/temp/'.$src);
		$int_img->crop(intval(Input::get('w')), intval(Input::get('h')), intval(Input::get('x')), intval(Input::get('y')));
		$int_img->fit(300, 300);
		$int_img->save($src_path);

		$people = People::where('id', '=', $id)->first();
		$people->people_pic = $src_path;
		$people->save();

		$photo = new Photo;
		$photo->path = $src_path;
		$photo->save();
		$people->photos()->save($photo);


		if($people && $photo){
			return Redirect::route('profile-people', array('id' => $id))
				->with('global-success', 'Successfully updated photo.');
		}else{
			return Redirect::route('profile-people', array('id' => $id))
					->with('global-error', 'Failed to upload, please try again.');
		}

		return Redirect::route('profile-people', array('id' => $id))
					->with('global-error', 'Failed to upload, please try again.');
		
	}

}