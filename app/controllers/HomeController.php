<?php

class HomeController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function home(){

		$today = Carbon::today(new DateTimeZone('Asia/Kuala_Lumpur'));
		$today = strtotime($today) * 1000;

		$tomorrow = Carbon::tomorrow(new DateTimeZone('Asia/Kuala_Lumpur'));
		$tomorrow = strtotime($tomorrow) * 1000;


		$specials = EEvent::where('start', '<', $tomorrow)
			->where('end', '>', $today)
			->get();

		$recommend = Site::where('id', '=', '1000000018')->first();

		$promotes = Site::where('id', '=', '1000000001')->get();


		return View::make('home')
			->with('specials', $specials)
			->with('recommend', $recommend)
			->with('promotes', $promotes);
	}

	public function getJoin(){
		return View::make('join');
	}

	public function postJoin(){
		$validator = Validator::make(Input::all(),
			array(
				'email' => 'required|email|unique:activates|unique:peoples',
				'password' => 'required|min:8'
			)
		);

		if($validator->passes()){
			$email = Input::get('email');
			$password = Input::get('password');

			//Activation code
			$code = str_random(60);


			$activate = Activate::create(array(
				'email' => $email,
				'password' => Hash::make($password),
				'code' => $code,
				'active' => 0
			));

			if($activate){
				Mail::send('emails.auth.activate',
					array('link' => URL::route('activate', $code)),
					function($message) use ($activate){
						$message->to($activate->email)->subject('Welcome to VCT! Please Activate Your Account');
					}
				);
				return Redirect::route('login')
					->with('success-join', "You've successfully created your VCT account!<br>Check your email to activate your account.");
			}
		}else{
			return Redirect::route('join')
				->withErrors($validator)
				->withInput();
		}
	}

	public function getActivate($code){
		$activate = Activate::where('code', '=', $code)->where('active', '=', 0);

		if($activate->count()){
			$activate = $activate->first();

			$activate->active = 1;
			$activate->code = '';

			if($activate->save()){
				People::create(array(
					'email' => $activate->email,
					'password' => $activate->password,
					'active' => 1
				));
				return Redirect::route('login')
					->with('success-join', "You've successfully activated your VCT account!<br>You can login now.");
			}
		}
		return Redirect::route('login')
			->with('fail-join', 'We could not activate your account. Try again later.');
	}

	public function getLogin(){
		return View::make('login');
	}

	public function postLogin(){
		$validator = Validator::make(Input::all(),
			array(
				'email' => 'required|email',
				'password' => 'required'
			)
		);
		if($validator->passes()){


			$auth = Auth::attempt(array(
				'email' => Input::get('email'),
				'password' => Input::get('password')
		));

			if($auth){
				$people = Auth::user();

				if($people->first_name == NULL){
					return Redirect::route('settings-profile')
						->with('global-success', 'Logged In, please enter your details.');
				}else{
					return Redirect::intended('/')
						->with('global-success', 'Logged In');
				}
				
			}else{
				return Redirect::route('login')
					->with('error-message', 'Wrong email or password');
			}
		}else{
			return Redirect::route('login')
				->withErrors($validator)
				->withInput();
		}
		return Redirect::route('login')
			->with('error-message', 'There was a problem loging you in.');
	}

	public function getForgot(){
		return View::make('forgot');
	}

	public function postForgot(){
		$validator = Validator::make(Input::all(),
			array(
				'email' => 'required|email'
		));

		if($validator->passes()){
			$people = People::where('email', '=', Input::get('email'));

			if($people->count()){
				$people = $people->first();

				if($people->active !=1){
					return Redirect::route('forgot')
						->with('global-error', 'Your account not yet activated.');
				}else{
					$code = str_random(60);
					$password = str_random(10);

					$people->code = $code;
					$people->password_temp = Hash::make($password);

					if($people->save()){
						Mail::send('emails.auth.forgot',
							array('link' => URL::route('recover', $code), 'password' => $password),
							function($message) use ($people){
								$message->to($people->email)->subject('Reset Your Password');
							});
						return Redirect::route('login')
							->with('success-join', 'An email with instructions has been sent to your email address. Please check your email.');
					}
				}
			}
		}else{
			return Redirect::route('forgot')
				->withErrors($validator);
		}
		return Redirect::route('forgot')
			->with('global-error', 'Could not request new password.');
	}

	public function getRecover($code){
		$people = People::where('code', '=', $code)
			->where('password_temp', '!=', '');

		if($people->count()){
			$people = $people->first();

			$people->password =$people->password_temp;
			$people->password_temp = '';
			$people->code = '';

			if($people->save()){
				return Redirect::route('login')
					->with('success-join', 'Your account has been recovered and you can sign in with your new password.');
			}
		}
		return Redirect::route('home')
			->with('global-error', 'Could not recover your account.');
	}

	public function getSearch($keywords){

		$q = $keywords;

		$searchKey = explode('+', $keywords);

		$query = DB::table('sites');


		foreach($searchKey as $keyword){
			$query->where('site_name', 'LIKE', '%' . $keyword . '%')
				->orWhere('about', 'LIKE', '%' . $keyword . '%');
		}

		$results = $query->orderBy('updated_at', 'desc')->get();

		$search = Search::create(array(
				'keyword' => $q,
			));

		return View::make('result', array('keyword' => $keywords))
			->with('results', $results)
			->with('q', $q);
	}

	public function postSearch(){

		if(Input::has('search')){
			$q = Input::get('search');
			$slug = Str::slug($q, '+');

			$searchKey = explode(' ', $q);



			$query = DB::table('sites');


			foreach($searchKey as $keyword){
				$query->where('site_name', 'LIKE', '%' . $keyword . '%')
					->orWhere('about', 'LIKE', '%' . $keyword . '%');
			}

			$results = $query->orderBy('updated_at', 'desc')->get();


			return Redirect::route('search', array('keyword' => $slug))
				->with('results', $results)
				->with('q', $q);
		}else{
			return Redirect::back();
		}

	}



}
