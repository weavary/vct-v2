<?php

class ApiController extends BaseController{
	public function getEvents(){
		//$events = EEvent::all()->toArray();;
		
		//$result = array();
		//foreach ($events as $event) {
         //   $result[] = array(
          //      'id' => $event->id,
         //       'event_name' => $event->event_name,
//);
       // }


        //$peoples= People::all()->toArray();;
        //$result1 = array();
        //foreach ($peoples as $people) {
        //    $result1[] = array(
         //       'id' => $people->id,
         //       'email' => $people->email,
         //   );
        //}

        $events = EEvent::all();

        return Response::json([
        	'success' => 1,
        	'result' => $events->toArray()
        ], 200);


	}

    public function getTags() {
        $query = Tag::where('tag_name', 'like', Input::get('term') . '%')->orderBy('tag_name')->get();
        foreach($query as $tag) $data[] = array('value' => $tag->tag_name);
        return Response::json($data);
    }

    public function postLogin(){
    	try{
	        $validator = Validator::make(Input::all(),
				array(
					'email' => 'required|email',
					'password' => 'required'
				)
			);
			if($validator->passes()){
				$auth = Auth::attempt(array(
					'email' => Input::get('email'),
					'password' => Input::get('password')
			    ));
	
				if($auth){
	                return Response::json(array('status' => 'success', 'status' => Auth::user()));
				}else{
	                return Response::json(array('status' => 'login-failed', 'status' => 'Wrong email or password'));
				}
			}else{
	            return Response::json(array('status' => 'failed', 'status' => $validator->messages()));
			}
    	}catch(Exception $e){
			return Response::json(array('status' => 'exception', 'result' => $e->getMessage()));
		}
    }

    public function getSearch(){
		try{
	        $validator = Validator::make(Input::all(),
				array(
					'q' => 'required',
				)
			);
			
			if ($validator->passes()){
				
				$keywords = Input::get('q');
		
				$searchKey = explode('+', $keywords);
		
				$query = DB::table('sites');
		
		
				foreach($searchKey as $keyword){
					$query->where('site_name', 'LIKE', '%' . $keyword . '%')
						->orWhere('about', 'LIKE', '%' . $keyword . '%');
				}
		
				$results = $query->orderBy('updated_at', 'desc')->get();
		/*
				$search = Search::create(array(
						'keyword' => $q,
					));
		*/
		        return Response::json(array('status' => 'success', 'result' => $results));
			}else{
	            return Response::json(array('status' => 'failed', 'data' => $validator->messages()));
			}
		}catch(Exception $e){
			return Response::json(array('status' => 'exception', 'result' => $e->getMessage()));
		}
    }
    
	public function listEvent(){

		try{
			$time_now = Carbon::now(new DateTimeZone('Asia/Kuala_Lumpur'));
			$time_now = strtotime($time_now) * 1000;
	
			$events = EEvent::where('end', '>=', $time_now)
				->orderBy('start', 'asc')->get();
	        return Response::json(array('status' => 'success', 'result' => $events));
		}catch(Exception $e){
			return Response::json(array('status' => 'exception', 'result' => $e->getMessage()));
		}
	}

	public function listNearby(){
		try{
			$validator = Validator::make(Input::all(),
				array(
					'long' => 'required',
					'lat' => 'required',
					'distance' => 'required'
				)
			);
			
			if($validator->passes()){
				$nearby = Site::whereBetween('lng', array(Input::get('long') - Input::get('distance'), Input::get('long') + Input::get('distance')))
				->whereBetween('lat', array(Input::get('lat') - Input::get('distance'), Input::get('lat') + Input::get('distance')))->get();
	        	return Response::json(array('status' => 'success', 'result' => $nearby));
			}else{
	            return Response::json(array('status' => 'failed', 'result' => $validator->messages()));
			}
		}catch(Exception $e){
			return Response::json(array('status' => 'exception', 'result' => $e->getMessage()));
		}
	}
	
	public function randomFoodSite(){
		try{
			$sites = Site::where('about', 'LIKE', '%food%')->orderBy(DB::raw('RAND()'))->take(1)->get();
	        return Response::json(array('status' => 'success', 'result' => $sites));
		}catch(Exception $e){
			return Response::json(array('status' => 'exception', 'result' => $e->getMessage()));
		}
	}
	
	public function listLatestSites(){
		try{
			$sites = Site::orderBy('created_at', 'desc')->take(10)->get();
			return Response::json(array('status' => 'success', 'result' => $sites));
		}catch(Exception $e){
			return Response::json(array('status' => 'exception', 'result' => $e->getMessage()));
		}	
	}
}