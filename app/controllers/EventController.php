<?php

class EventController extends BaseController {

	public function event($id){
		$event = EEvent::where('id', '=', $id)->first();
		if($event){
		$url = Request::url();

		$site = EEvent::find($id)->site;



		$admin = NULL;
		if(Auth::check()){
			$people = People::find(Auth::user()->id);
			$admin= $people->sites->contains($event->site_id);
		}

		/* Pizza Kickstarter */
		if($event->id == 1){
			$time_now = Carbon::now(new DateTimeZone('Asia/Kuala_Lumpur'));

			$date = strtotime("November 10, 2014 12:00 AM");

			$time_now= Carbon::parse($time_now)->timestamp;
			$count_down = $date - $time_now;
			$count_down = floor($count_down / 86400);


			return View::make('events.pizza')
				->with('count_down', $count_down);
		}

		

		
			return View::make('events.index')
				->with('site', $site)
				->with('event', $event)
				->with('url', $url)
				->with('admin', $admin);
		}else{
			return App::abort(404);

		}
	}

	public function allEvent(){
		return View::make('events.all');
	}

	public function listEvent(){

		$time_now = Carbon::now(new DateTimeZone('Asia/Kuala_Lumpur'));
		$time_now = strtotime($time_now) * 1000;

		$events = EEvent::where('end', '>=', $time_now)
			->orderBy('start', 'asc')->get();
		return View::make('events.list')
			->with('events', $events);
	}

	public function postCreateEvent($site_id){


		$validator = Validator::make(Input::all(),
			array(
				'event_name' => 'required|min:3',
				'location' => 'required|min:3',
				'start_date' => 'required',
							
		));
		
		if($validator->passes()){
			$event_name = Input::get('event_name');
			$event_desc = Input::get('event_desc');
			$location = Input::get('location');
			$start_date = Input::get('start_date');
			$start_time = Input::get('start_time');

			$start_date = Carbon::createFromFormat('d/m/Y', $start_date)->toDateString();
			$start = $start_date.' '.$start_time;
			$start = strtotime($start) * 1000;


			if(Input::has('end_date')){
				$end_date = Input::get('end_date');
				$end_time = Input::get('end_time');

				$end_date = Carbon::createFromFormat('d/m/Y', $end_date)->toDateString();
				$end = $end_date.' '.$end_time;
				$end = strtotime($end) * 1000; 
			}else{
				$end = $start_date.' 23.30';
				$end = strtotime($end) * 1000; 
			}
			



			$event = EEvent::create(array(
					'event_name' => $event_name,
					'event_desc' => $event_desc,
					'location' => $location,
					'event_pic' => 'images/index.jpg',
					'start' => $start,
					'end' => $end,
					'site_id' => $site_id,
					'people_id' => Auth::user()->id,

			));

			if($event){
				return Redirect::route('profile-event', array('id' => $event->id));
			}else{
				return Redirect::back()
					->with('global-error', 'Unable to create event.');
			}
		}return Redirect::back()
			->withErrors($validator)
    		->withInput()
			->with('global-error', 'Unable to create event.');

		

	}

	public function getSettingsEvent($id){
		$event = EEvent::where('id', '=', $id)->first();

		$url = Request::url();

		$people = People::find(Auth::user()->id);;
		$admin= $people->sites->contains($event->site_id);

		return View::make('events.edit')
			->with('event', $event)
			->with('url', $url)
			->with('admin', $admin);
	}

	public function postSettingsEvent($id){
		$validator = Validator::make(Input::all(),
			array(
				'event_name' => 'required',
				'location' => 'required|min:3',
				'start_date' => 'required',		
		));

		$event= EEvent::where('id', '=', $id)->first();

		if($validator->passes()){
			$event_name = Input::get('event_name');
			$event_desc = Input::get('event_desc');
			$location = Input::get('location');
			$start_date = Input::get('start_date');
			$start_time = Input::get('start_time');

			$start_date = Carbon::createFromFormat('d/m/Y', $start_date)->toDateString();
			$start = $start_date.' '.$start_time;
			$start = strtotime($start) * 1000;


			if(Input::has('end_date')){
				$end_date = Input::get('end_date');
				$end_time = Input::get('end_time');
				$end_date = Carbon::createFromFormat('d/m/Y', $end_date)->toDateString();
				$end = $end_date.' '.$end_time;
				$end = strtotime($end) * 1000; 
			}else{
				$end = $start_date.' 23.30';
				$end = strtotime($end) * 1000; 
			}

			$event->event_name = $event_name;
			$event->event_desc = $event_desc;
			$event->location = $location;
			$event->start = $start;
			$event->end = $end;
			$event->save();

			if($event){
				return Redirect::route('edit-event', array('id' => $event->id))
					->with('global-success', 'Update site info.');
			}
		}else{
			return Redirect::route('edit-site', array('id' => $event->id))
				->withErrors($validator)
				->withInput();
		}
	}

	public function postUploadPhotoEvent($id){
		$validation = Validator::make(Input::all(), 
			array(
				'image' => 'required|mimes:jpeg,png,bmp,gif|max:15360'
		));

		$event = EEvent::where('id', '=', $id)->first();
		
		if($validation->passes()){
			$file = Input::file('image');
			$org_filename = $file->getClientOriginalName();
			$ext = explode("/", $file->getMimeType());
			$ext = $ext[1];
			
			/* filename */
			$unique_event_id = crc32($event->id);
			$unique_filename = crc32($org_filename);
			$unique_user_id = crc32(Auth::user()->id);

			$final_filename = time() .'_'.$unique_event_id.'_'.$unique_filename.'_'.$unique_user_id.'.'.$ext;
			$final_filepath = "photos/temp/" . $final_filename;
			$width = Image::make($file)->width();

			if($width >= 900){
				$width = 900;
			}else{
				$width = $width;
			}



			if($file){
				Image::make($file->getRealPath())->resize($width, null, function ($constraint) {$constraint->aspectRatio();})->save($final_filepath);
				return Redirect::route('crop-photo-event', array('id' => $id))
					->with('image', $final_filename)
					->with('event', $event);
			}
			else{
				return Redirect::route('profile-event', array('id' => $id))
					->with('global-error', 'Failed to upload, please try again.');
			}
		}
		else{
			return Redirect::route('profile-event', array('id' => $id))
				->withErrors($validation);
		}
	}

	public function getCropPhotoEvent($id){
		$event = EEvent::where('id', '=', $id)->first();

		$admin = NULL;
		if(Auth::check()){
			$people = People::find(Auth::user()->id);;
			$admin= $people->sites->contains($event->site_id);
		}

		return View::make('events.crop', array('id' => $id))
			->with('event', $event)
			->with('admin', $admin);
		
	}

	public function postCropPhotoEvent($id){

		$src = Input::get('image');
		$ext = pathinfo($src, PATHINFO_EXTENSION);

		$src_path = "photos/" . basename($src, "." . $ext) . '_c.' . $ext;
		
		$int_img = Image::make('photos/temp/'.$src);
		$int_img->crop(intval(Input::get('w')), intval(Input::get('h')), intval(Input::get('x')), intval(Input::get('y')));
		$int_img->fit(720, 405);
		$int_img->save($src_path);

		$event = EEvent::where('id', '=', $id)->first();
		$event->event_pic = $src_path;
		$event->save();

		$photo = new Photo;
		$photo->path = $src_path;
		$photo->save();
		$event->photos()->save($photo);


		if($event && $photo){
			return Redirect::route('profile-event', array('id' => $id))
				->with('global-success', 'Successfully updated photo.');
		}else{
			return Redirect::route('profile-event', array('id' => $id))
					->with('global-error', 'Failed to upload, please try again.');
		}

		return Redirect::route('profile-event', array('id' => $id))
					->with('global-error', 'Failed to upload, please try again.');
		
	}

	public function getEventPayment($id, $amount){
		if($id == 1){


	                     

			return View::make('events.payment')
				->with('amount', $amount)
				->with('id', $id);	
		}else{
			return App::abort(404);
		}

	
	}

	public function postEventPayment($id, $amount){
		if($amount >= 100){
			$validator = Validator::make(Input::all(),
				array(
					'name' => 'required|min:3',
					'phone' => 'required',
					'email' => 'required|email',
					'receipt' => 'required',
					'address' => 'required',
					'postcode' => 'required|size:5|digits:5',
					'city' => 'required',
					'tshirt' => 'required',
			));
		}else{
			$validator = Validator::make(Input::all(),
				array(
					'name' => 'required|min:3',
					'phone' => 'required',
					'email' => 'required|email',
					'receipt' => 'required',
			));
		}

		

		if($validator->passes()){
			$name = Input::get('name');
			$email = Input::get('email');
			$phone = Input::get('phone');
			$pledge = Input::get('pledge');

			$address = NULL;
			$postcode = NULL;
			$city = NULL;
			$tshirt = Input::get('tshirt');
			$vege = NULL;

			if($amount >= 10){
				$vege = Input::get('vege');
			}

			if($amount >= 100){
				$address = Input::get('address');
				$postcode = Input::get('postcode');
				$city = Input::get('city');
				$tshirt = Input::get('tshirt');
			}


			$file = Input::file('receipt');

			$ext = $file->getClientOriginalExtension();

			$payment_id = crc32($id);


			$receipt_name = time() .'_'. $payment_id.'.'.$ext;
			$receipt_path = "files/" . $receipt_name;

			Input::file('receipt')->move('files/', $receipt_name);

			$pizza = Pizza::create(array(
				'name' => $name,
				'email' => $email,
				'phone' => $phone,
				'receipt' => $receipt_path,
				'pledge' => $pledge,
				'address' => $address,
				'postcode' => $postcode,
				'city' => $city,
				'tshirt' => $tshirt,
				'vege' => $vege,
			
			));


			if($pizza){

				Mail::send('emails.auth.thankpizza', 
					array('amount' => $pledge, 'name' => $name), 
					function($message)use ($pizza){
				   	 $message->to($pizza->email, $pizza->name)->subject('Thank You for Pledging VCT');
				});
				return Redirect::route('profile-event', array('id' => $id))
					->with('pizza-success', 'Thank You');
			}

		}else{
			return Redirect::back()
				->withErrors($validator)
				->withInput();
		}
	}


}
