<?php

class SettingController extends BaseController {

	public function settings(){
		return View::make('settings.index');
	}

	public function logout(){
		Auth::logout();
		return Redirect::back()
			->with('global-success', 'Logout');
	}

	public function getChangeProfile(){
		return View::make('settings.index');
	}

	public function postChangeProfile(){
		$validator = Validator::make(Input::all(),
			array(
				'first_name' => 'required|min:2',
				'last_name' => 'required|min:2',
		));

		$people = Auth::user();

		if($validator->passes()){
			$first_name = Input::get('first_name');
			$last_name = Input::get('last_name');




			$people->first_name = $first_name;
			$people->last_name = $last_name;
			$people->save();

			if($people){
				return Redirect::route('settings-profile')
					->with('global-success', 'Updated profile info.');
			}
		}else{
			return Redirect::route('settings-profile')
				->withErrors($validator)
				->withInput();
		}

	}

	public function getChangePassword(){
		return View::make('settings.password');
	}

	public function postChangePassword(){
		$validator = Validator::make(Input::all(),
			array(
				'current_password' => 'required',
				'password' => 'required|min:8',
				'confirm_password' => 'required|same:password'
		));

		if($validator->passes()){

			$people = People::find(Auth::user()->id);

			$current_password = Input::get('current_password');
			$password = Input::get('password');

			if(Hash::check($current_password, $people->getAuthPassword())){
				$people->password = Hash::make($password);

				if($people->save()){
					return Redirect::route('settings-password')
						->with('global-success', 'Password successfully changed.');
				}
			}else{
				return Redirect::route('settings-password')
					->with('global-error', 'Your password is incorrect.');
			}
		}else{
			return Redirect::route('settings-password')
				->withErrors($validator);
		}
		return Redirect::route('settings-password')
			->with('global-error', 'Your password could not be changed.');
	}

}
